
'use strict';

const Log = require('./log');
const Clc = require('cli-color');
const Fs = require('fs');
const Path = require('path');


class RepoManager {
  constructor(buildLibrary) {
    this.buildLibrary = buildLibrary;
    this.globalReposFile = true;
    this.repos = null;
    this.repoCache = new Map();
    this.developmentRepo = '';
    this.candidateRepo = '';
    this.deliveryRepo = '';
    this.shadowDevelopmentRepo = '';
    this.shadowCandidateRepo = '';
    this.shadowDeliveryRepo = '';
    this.repoGroups = null;
  }
  
  init(cb, globalReposFile = true,) {
    this.globalReposFile = globalReposFile;
    this._load((err) => {
      cb(err);
    });
  }
  
  onlyExportedRepos() {
    this.repos.repoGroups[0].repos.forEach((repo) => {
      repo.dependencies.forEach((dependency) => {
        if(!dependency.export) {
          this.repoCache.delete(dependency.name);
        }
      });
    });
  }
  
  getPath() {
    if(this.buildLibrary && this.globalReposFile) {
      return Path.resolve(`..${Path.sep}z-build-build${Path.sep}project${Path.sep}server${Path.sep}repos.json`);
    }
    else {
      return Path.resolve(`.${Path.sep}repos.json`);
    }
  }
  
  get(repoName) {
    const repoData = this.repoCache.get(repoName);
    if(repoData) {
      return repoData;
    }
    else {
      Log.log(Clc.red('Repo'), repoName, Clc.red('is not configured in'), this.getPath() + Clc.red('.'));
    }
  }
    
  _parse(data) {
    try {
      this.repos = JSON.parse(data);
    }
    catch(err) {
      Log.log(`Can not parse file '${this.getPath()}'.`, err);
      return err;
    }
    return null;
  }
  
  _load(cb) {
    Fs.readFile(this.getPath(), (err, data) => {
      if(err) {
        this.cb(new Error(`Could not read file: '${this.getPath()}'`));
      }
      else {
        const error = this._parse(data);
        if(!error) {
          this.developmentRepo = this.repos.developmentRepo;
          this.candidateRepo = this.repos.candidateRepo;
          this.deliveryRepo = this.repos.deliveryRepo;
          this.shadowDevelopmentRepo = this.repos.shadowDevelopmentRepo;
          this.shadowCandidateRepo = this.repos.shadowCandidateRepo;
          this.shadowDeliveryRepo = this.repos.shadowDeliveryRepo;
          this.repos.repoGroups.forEach((repoGroup) => {  
            const path = repoGroup.path;
            repoGroup.repos.forEach((repoData) => {
              this.repoCache.set(repoData.folder, {
                path: path,
                data: repoData,
                packageJson: null
              });
            });
          });
          cb();
        }
        else {
          cb(error);
        }
      }
    });
  }
}

module.exports = RepoManager;
