
'use strict';

const Build = require('./build');
const Log = require('./log');
const Clc = require('cli-color');
const ChildProcess = require('child_process');
const Fs = require('fs');


class BuildServerRc extends Build {
  constructor(appName, part, source, dest) {
    super(appName, `server-${part}-rc`, __filename, source, dest);
    this.part = part;
  }
  
  saveData() {
    return {
      part: this.part,
      source: this.source,
      dest: this.dest
    };
  }
  
  task(cb) {
    Fs.access(this.dest, Fs.constants.F_OK, (err) => {
      if(!err) {
        this.execute(0, 0, cb);
      }
      else if('ENOENT' === err.code) {
        Fs.mkdir(this.dest, {recursive: true}, (err) => {
          this.execute(0, 0, cb);
        });
      }
      else {
        cb(err);
      }
    });
  }
  
  execute(retries, code, cb) {
    if(2 >= retries) {
      const childProcess = ChildProcess.fork(require.resolve('./child-server-rc.js'), [JSON.stringify([this.appName, this.name, this.silent, this.repo, this.source, this.dest, process.env.NODE_ENV])], {execArgv: []});
      let done = false;
      childProcess.on('exit', (code) => {
        if(!done) {
          if(0 !== code) {
            return this.execute(++retries, code, cb);
          }
          done = true;
          cb();
        }
      });
      childProcess.on('message', (code) => {
        if(!done && 0 === code) {
          done = true;
          cb();
        }
      });
    }
    else {
      Log.log(Clc.red(`${this.name} - build-server-rc: code`), code);
      cb(new Error(`Failed to build ${this.name} - build-server-rc.`));
    }
  }
}

module.exports = BuildServerRc;
