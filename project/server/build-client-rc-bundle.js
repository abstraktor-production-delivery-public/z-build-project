
'use strict';

const Build = require('./build');
const Log = require('./log');
const Clc = require('cli-color');
const ChildProcess = require('child_process');
const Fs = require('fs');


class BuildClientRcBundle extends Build {
  constructor(appName, part, source, dest, remove) {
    super(appName, `client-css-${part}-bundle`, __filename, source, dest);
    this.part = part;
    this.remove = remove;
    this.bundleName = `bundle-${part}.css`;
    this.buildEnv = 'development' === Reflect.get(global, 'release-data@abstractor').env ? 0 : 1;
  }
  
  saveData() {
    return {
      part: this.part,
      source: this.source,
      dest: this.dest,
      remove: this.remove
    };
  }
  
  task(cb) {
    Fs.access(this.dest, Fs.constants.F_OK, (err) => {
      if(!err) {
        this.execute(0, 0, cb);
      }
      else if('ENOENT' === err.code) {
        Fs.mkdir(this.dest, {recursive: true}, (err) => {
          this.execute(0, 0, cb);
        });
      }
      else {
        cb(err);
      }
    });
  }
  
  execute(retries, code, cb) {
    if(2 >= retries) {
      const childProcess = ChildProcess.fork(require.resolve('./child-client-rc-bundle.js'), [JSON.stringify([this.appName, this.name, this.silent, this.bundleName, this.repo, this.source, this.dest, this.remove, process.env.NODE_ENV, this.buildEnv])], {execArgv: []});
      let done = false;
      childProcess.on('exit', (code) => {
        if(!done) {
          if(0 !== code) {
            return this.execute(++retries, code, cb);
          }
          done = true;
          cb();
        }
      });
      childProcess.on('message', (code) => {
        if(!done && 0 === code) {
          done = true;
          cb();
        }
      });
    }
    else {
      Log.log(Clc.red(`${this.name} - build-client-rc-bundle: code`), code);
      cb(new Error(`Failed to build ${this.name} - build-client-rc-bundle.`));
    }
  }
}

module.exports = BuildClientRcBundle;
