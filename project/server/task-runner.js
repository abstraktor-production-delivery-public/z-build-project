
'use strict';

const Log = require('./log');
const FastGlob = require('fast-glob');
const Chokidar = require('chokidar');
const Path = require('path');


class TaskRunner {
  static TASK = 0;
  static SERIAL = 1;
  static PARALLEL = 2;
  static ANONYMOUS = '<anonymous>';
  
  constructor() {
    this.tasks = new Map();
  }
  
  task(name, fTask) {
    if('string' === typeof name) {
      if(this.tasks.has(name)) {
        return this._runnerTask(TaskRunner.ANONYMOUS, name);
      }
      else {
        this.tasks.set(name, {
          fTask: fTask,
          type: TaskRunner.TASK
        });
        return this._runnerTask(name, fTask);
      } 
    }
    else if('function' === typeof name) {
      return this._runnerTask(TaskRunner.ANONYMOUS, name);
    }
    else {
      const error = 'Not allowed type: ' + (typeof name);
      Log.log(error);
    }
  }
  
  serial(name, ...fTasks) {
    if('string' === typeof name) {
      if(this.tasks.has(name)) {
        const tasks = [name, ...fTasks]
        return this._runnerSerial(TaskRunner.ANONYMOUS, ...tasks);
      }
      else {
        this.tasks.set(name, {
          fTask: fTasks,
          type: TaskRunner.SERIAL
        });
        return this._runnerSerial(name, ...fTasks);
      }
    }
    else if('function' === typeof name) {
      const tasks = [name, ...fTasks]
      return this._runnerSerial(TaskRunner.ANONYMOUS, ...tasks);
    }
    else {
      const error = 'Not allowed type: ' + (typeof name);
      Log.log(error);
    }
  }
  
  parallel(name, ...fTasks) {
    if('string' === typeof name) {
      if(this.tasks.has(name)) {
        const tasks = [name, ...fTasks]
        return this._runnerParallel(TaskRunner.ANONYMOUS, ...tasks);
      }
      else {
        this.tasks.set(name, {
          fTask: fTasks,
          type: TaskRunner.PARALLEL
        });
        return this._runnerParallel(name, ...fTasks);
      } 
    }
    else if('function' === typeof name) {
      const tasks = [name, ...fTasks]
      return this._runnerParallel(TaskRunner.ANONYMOUS, ...tasks);
    }
    else {
      const error = 'Not allowed type: ' + (typeof name);
      Log.log(error);
    }
  }
  
  watch(name, paths, options, cb) {
    const resolvedPaths = Array.isArray(paths) ? paths : [paths];
    let changed = false;
    Chokidar.watch(resolvedPaths, options).on('all', (event, changedPath, a, b) => {
      if(!changed) {
        changed = true;
        setTimeout(() => {
          changed = false;
          cb();
        }, 50);
      }
    });
  }
  
  run(nameOrTask, cb) {
    if('string' === typeof nameOrTask) {
      if(!this.tasks.has(nameOrTask)) {
        const error = 'Task ' + nameOrTask + ' is not registered';
        Log.log(error);
        if(cb) {
          process.nextTick(() => {
            cb(new Error(error));
          });
        }
      }
      else {
        const task = this.tasks.get(nameOrTask);
        if(TaskRunner.TASK === task.type) {
          this._runnerTask(nameOrTask, task.fTask)(cb);
        }
        else if(TaskRunner.SERIAL === task.type) {
          this._runnerSerial(nameOrTask, ...task.fTask)(cb);
        }
        else if(TaskRunner.PARALLEL === task.type) {
          this._runnerParallel(nameOrTask, ...task.fTask)(cb);
        }
        else {
          const error = 'Not allowed runner: ' + task.type;
          Log.log(error);
          if(cb) {
            process.nextTick(() => {
              cb(new Error(error));
            });
          }
        }
      }
    }
    else if('function' === typeof nameOrTask) {
      process.nextTick(() => {
        nameOrTask(cb);
      });
    }
    else {
      const error = 'Not allowed type: ' + (typeof nameOrTask);
      Log.log(error);
      process.nextTick(() => {
        cb(new Error(error));
      });
    }
  }
  
  _runnerTaskName(name, cb) {
    if(!this.tasks.has(name)) {
      process.nextTick(() => {
        Log.log('Task', name, 'is not registered');
        if(cb) {
          cb(new Error('Task', name, 'is not registered'));
          return;
        }
      });
    }
    else {
      process.nextTick(() => {
        const task = this.tasks.get(name);
        if(TaskRunner.TASK === task.type) {
          try {
            task.fTask(cb);
     		  }
		      catch(err) {
            Log.log(err);
          }
        }
        else {
          Log.log('NOT IMPLEMENTED !!!');
        }
      });
    }
  }
  
  _runnerTask(name, fTask) {
    return (cb) => {
      if(fTask) {
        if('string' === typeof fTask) {
          this._runnerTaskName(fTask, cb);
        }
        else if('function' === typeof fTask) {
          process.nextTick(() => {
            try {
              fTask((err) => {
                if(cb) {
                  cb(err);
                }
              });
            }
		        catch(err) {
              Log.log(err);
            }
          });
        }
        else {
          const error = 'Not allowed type: ' + (typeof fTask);
          Log.log(error);
		      if(cb) {
            process.nextTick(() => {
              cb(new Error(error));
            });
          }
        }
      }
      else {
        this._runnerTaskName(name, cb);
      }
    };
  }
  
  _runnerSerial(name, ...fTasks) {
    return (cb) => {
      let index = -1;
      const fCallback = (err) => {
        if(err) {
          if(cb) {
            process.nextTick(() => {
              cb(err);
            });
          }
          return;
        }
        if(++index < fTasks.length) {
          const task = fTasks[index];
          if(task) {
            this._runnerTask(name, task)(fCallback);
          }
          else {
            process.nextTick(() => {
              fCallback();
            });
          }
        }
        else {
          if(cb) {
            process.nextTick(() => {
              cb();
            });
          }
        }
      };
      fCallback();
    };
  }
  
  _runnerParallel(name, ...fTasks) {
    return (cb) => {
      let foundError = false;
      let pendings = fTasks.length;
      const fCallback = (err) => {
        if(!foundError) {
          if(err) {
            foundError = true;
            if(cb) {
              process.nextTick(() => {
                cb(err);
              });
            }
          }
          else {
            if(0 === --pendings) {
              if(cb) {
                process.nextTick(() => {
                  cb();
                });
              }
            }
          }
        }
      };
      fTasks.forEach((task) => {
        if(task) {
          this._runnerTask(name, task)(fCallback);
        }
        else {
          process.nextTick(() => {
            fCallback();
          });
        }
      });
    };
  }
}


module.exports = TaskRunner;
