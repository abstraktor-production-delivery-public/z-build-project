
'use strict';

const ChildBuildData = require('./child-build-data');
const Log = require('./log');
const TaskRunnerGlob = require('./task-runner-glob');
const Clc = require('cli-color');
const Fs = require('fs');
const Path = require('path');


class ChildServerRc {
  constructor(appName, name, silent, repo, source, dest, nodeEnv) {
    this.appName = appName;
    this.name = name;
    this.silent = silent;
    this.repo = repo;
    this.source = source;
    this.dest = dest;
    process.env.NODE_ENV = nodeEnv;
  }

  task(cb) {
    const pendings = {
      val: 0
    };
    const buildDataPath = Path.resolve(`..${Path.sep}Generated${Path.sep}Build${Path.sep}${this.appName}-${this.name.replaceAll(':', '-').replaceAll('/', '-')}`);
    const resolvedPath = Path.resolve(this.repo ? Path.resolve(this.repo) : '.');
    ChildBuildData.getBuildData(buildDataPath, (buildData) => {
      TaskRunnerGlob.glob(this.source, this.dest, resolvedPath, cb, (srcFile, dstFile, cb) => {
        let shallBuild = true;
        ++pendings.val;
        Fs.lstat(dstFile, (err, stat) => {
          if(shallBuild) {
            if(!!err && 'ENOENT' === err.code) {
              shallBuild = false;
              this._build(srcFile, dstFile, buildDataPath, buildData, pendings, cb);
            }
            else {
              if(0 === --pendings.val) {
                ChildBuildData.setBuildData(buildDataPath, buildData, cb);
              }
            }
          }
          else {
            if(0 === --pendings.val) {
              ChildBuildData.setBuildData(buildDataPath, buildData, cb);
            }
          }
        });
        ++pendings.val;
        Fs.lstat(srcFile, (err, stat) => {
          if(shallBuild) {
            const mTimeMs = buildData.map.get(srcFile);
            if(mTimeMs === stat.mtimeMs) {
              if(0 === --pendings.val) {
                ChildBuildData.setBuildData(buildDataPath, buildData, cb);
              }
            }
            else {
              shallBuild = false;
              this._build(srcFile, dstFile, buildDataPath, buildData, pendings, cb);
            }
          }
          else {
            if(0 === --pendings.val) {
              ChildBuildData.setBuildData(buildDataPath, buildData, cb);
            }
          }
        });
      });
    });
  }

  _build(srcFile, dstFile, buildDataPath, buildData, pendings, cb) {
    Fs.readFile(srcFile, (err, data) => {
      if(err) {
        Log.log(Clc.red(`Could not read: '`) + srcFile + Clc.red(`'.`), err);
        if(0 === --pendings.val) {
          cb();
        }
      }
      else {
        TaskRunnerGlob.verifyPath(dstFile, (err) => {
          if(err) {
            if(0 === --pendings.val) {
              cb();
            }
          }
          else {
            ChildBuildData.write(srcFile, dstFile, data, buildData, () => {
              if(0 === --pendings.val) {
                ChildBuildData.setBuildData(buildDataPath, buildData, cb);
              }
            });
          }
        });
      }
    });
  }
}


process.nextTick(() => {
  const parameters = JSON.parse(process.argv[2]);
  const childServerRc = new ChildServerRc(...parameters);
  childServerRc.task(() => {
    process.send(0);
  });
});
