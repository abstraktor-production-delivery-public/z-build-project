
'use strict';

const Log = require('./log');
const Workspace = require('./workspace');
const Clc = require('cli-color');
const Tr = Reflect.get(global, 'tr@abstractor');
const Fs = require('fs');
const Path = require('path');


class BuildAll {
  constructor(task, build, dev, organization, bottleneck, silent) {
    this.task = task;
    this.build = build;
    this.dev = dev;
    this.organization = organization;
    this.bottleneck = bottleneck;
    this.silent = silent;
    this.buildData = [];
    this.workspace = null;
    this.basePath = `${process.cwd()}${Path.sep}..`;
  }
  
  run(cb) {
    Fs.readdir(this.basePath, (err, files) => {
      if(err) {
        return cb(err);
      }
      if(undefined === files || 0 === files.length) {
        return cb();
      }
      let pendings = 0;
      if(0 === files.length) {
        cb();
      }
      files.forEach((dir) => {
        Fs.lstat(`${this.basePath}${Path.sep}${dir}`, (err, stat) => {
          if(stat.isDirectory()) {
            if(dir.startsWith('actor') || dir.startsWith('node-') || dir.startsWith('persistant-node-')) {
              ++pendings;
              Fs.readFile(`${this.basePath}${Path.sep}${dir}${Path.sep}settings.json`, 'utf8', (err, data) => {
                let settings = null;
                if(!err) {
                  try {
                    settings = JSON.parse(data);
                  }
                  catch(err) {
                    Log.log('Could not parse settings.json for', dir);
                  }
                  this.buildData.push({
                    name: dir,
                    settings: settings
                  });
                }
                else {
                  
                }
                if(0 === --pendings) {
                  this.buildAll(cb);
                }
              });
            }
          }
        });
      });
    });
  }
  
  buildAll(cb) {
    this.buildData.sort((a, b) => {
      return a.name > b.name ? 1 : -1;
    });
    let pendings = this.buildData.length;
    if(0 === pendings) {
      Log.log('Nothing to build.');
      return cb();
    }
    
    let index = 0;
    this.buildHandler(index, cb)
  }
  
  buildHandler(index, cb) {
    this.buildWorkspace('', index, () => {
      if(++index !== this.buildData.length) {
        this.buildHandler(index, cb);
      }
      else {
        cb();
      }
    });    
  }
  
  buildWorkspace(appName, index, cb) {
    const serverName = this.buildData[index].settings.type ? `${this.buildData[index].settings.type}-${this.buildData[index].settings.name}` : this.buildData[index].settings.name;
    Tr.task(serverName, (cbTask) => {
      cbTask();
    });
    Tr.serial((cbTask) => {
      Log.log(Clc.yellow('**************************************************'));
      Log.log(Clc.yellow(`*** Building: ${this.buildData[index].name}`));
      Log.log(Clc.yellow('**************************************************'));
      this.workspace = new Workspace(appName, this.buildData[index].name, this.task, this.build, this.dev, this.organization, this.bottleneck, this.silent);
      try {
        process.chdir(`${this.basePath}${Path.sep}${this.buildData[index].name}`);
        this.workspace.load((err) => {
          if(err) {
            Log.error('Could not start the Node:', err);
            return cbTask(err);
          }
          cbTask();
          process.chdir(this.basePath);
          cb();
        }, `task:${this.task}`);
      }
      catch(a) {
        Log.log(a);
      }
    })();
  }
}


module.exports = BuildAll;
