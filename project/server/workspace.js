
'use strict';

const Log = require('./log');
const BottleneckQueue = require('./bottleneck-queue');
const Project = require('./project');
const Build = require('./build');
const Task = require('./task');
const Clc = require('cli-color');
const Tr = Reflect.get(global, 'tr@abstractor');
const Fs = require('fs');
const Path = require('path');


class Workspace {
  constructor(appName, name, task, build, dev, organization, bottleneck, silent = false) {
    this.appName = appName;
    this.name = name;
    this.task = task;
    this.dev = dev;
    this.organization = organization;
    this.build = build;
    this.bottleneck = bottleneck;
    this.silent = silent;
    this.builds = [];
    this.plusServices = [];
    this.tasks = [];
    this.bottleneckQueue = new BottleneckQueue(bottleneck);
  }
  
  load(cbO, ...tasks) {
    const cb = () => {
      this.init();
      try {
        Tr.serial(...tasks)(cbO);
      }
      catch(err) {
        Log.log(Clc.red('Could not start'), err);
      }
    };
    let workspace = null;
    Fs.readFile(`.${Path.sep}workspace${Path.sep}${this.name}.wrk`, (err, data) => {
      if(err) {
        return Log.log(`workspace.read: '.${Path.sep}workspace${Path.sep}${this.name}.wrk' ERROR:`, err);
      }
      try {
        workspace = JSON.parse(data);
      }
      catch(error) {
        return Log.log(Clc.red('Could not load workspace.'), error);
      }
      const dependencyNameMap = new Map();
      let pendings = 0;
      const done = () => {
        this.builds.forEach((build) => {
          const dependencyNames = dependencyNameMap.get(build.name);
          dependencyNames.forEach((dependencyName) => {
            const dependentBuild = this.getBuild(dependencyName);
            if(!dependentBuild) {
              const plusServiceFound = this.plusServices.find((plusService) => {
                return plusService.buildName.startsWith(dependencyName);
              });
              if(plusServiceFound && plusServiceFound.loaded) {
                Log.log(Clc.red('Build'), build.name, Clc.red('can not load dependency build ') + dependencyName + Clc.red(`.`));
              }
            }
            else {
              build.addDependency(dependentBuild);
            }
          });
        });
      };
      const workspaceProjects = [];
      workspace.projects.forEach((projectData) => {
        if(!Array.isArray(projectData.type)) {
          workspaceProjects.push({
            projectData: projectData,
            type: projectData.type
          });
        }
        else {
          projectData.type.forEach((projectType) => {
            workspaceProjects.push({
              projectData: projectData,
              type: projectType
            });
          });
        }
      });
      workspaceProjects.forEach((workspacepProjectData) => {
        const projectData = workspacepProjectData.projectData;
        ++pendings;
        Project.load(projectData, workspacepProjectData.type, this.dev, this.organization, (projectBuilds) => {
          let buildPendings = projectBuilds.length;
          if(0 === buildPendings) {
            --pendings;
          }
          for(let i = 0; i< projectBuilds.length; ++i) {
	          const projectBuild = projectBuilds[i];
            if(0 === --buildPendings) {
              --pendings;
            }
            if(projectData.optional) {
              this.plusServices.push({
                name: projectData.serviceName,
                buildName: projectData.buildName,
                loaded: !projectBuild.error
              });
            }
            if(projectBuild.error) {
              if(projectData.optional) {
                Log.log(Clc.yellow('Service'), projectData.serviceName, Clc.yellow('can be ordered at Plus Services.'));
              }
              else {
                Log.log(Clc.red(`${projectData.name}:`), projectBuild.error);
              }
              if(0 === pendings) {
                done();
                cb();
              }
            }
            if(projectBuild.buildData) {
              projectBuild.buildData.builds.forEach((buildData) => {
                if('clientServer' === workspace.type || buildData.type === workspace.type) {
                  ++pendings;
                  const build = projectData.new ? `_build${Path.sep}` : '';
                  Build.load(this.appName, `${projectBuild.path}${Path.sep}${build}${buildData.fileName}.bld`, buildData.requireName, this.bottleneckQueue, (err, build, dependencyNames) => {
                    if(err) {
                      Log.log(Clc.red('Build.load'), err);
                    }
                    else {
                      build.setRepo(this.organization, buildData.repo, !!projectData.plugin, buildData.repoPath, buildData.local, this.dev);
                      dependencyNameMap.set(build.name, dependencyNames);
                      this.addBuild(build);
                    }
                    if(0 === --pendings) {
                      done();
                      cb();
                    }
                  });
                }
              });
            }
          }
        });
      });
      workspace.builds.forEach((buildData) => {
        ++pendings;
        Build.load(this.appName, `.${Path.sep}workspace${Path.sep}${buildData.fileName}.bld`, buildData.requireName, this.bottleneckQueue, (err, build, dependencyNames) => {
          if(err) {
            return Log.log(err);
          }
          if(null !== buildData.repo) {
            ++pendings;
            let repo = '';
            if(this.dev || buildData.local) {
              repo = Path.resolve(`../${buildData.repoPath}/${buildData.repo}`);
            }
            else {
              const organizationPath = this.organization ? `${this.organization}${Path.sep}` : '';
              repo = Path.resolve(`./node_modules/${organizationPath}${buildData.repo}`);
            }
            build.setRepo(this.organization, buildData.repo, false, buildData.repoPath, buildData.local, this.dev);
            if(0 === --pendings) {
              done();
              cb();
            }
          }
          else {
            build.setRepo(this.organization, null, false);
          }
          dependencyNameMap.set(build.name, dependencyNames);
          this.addBuild(build);
          if(0 === --pendings) {
            done();
            cb();
          }
        });
      });
      workspace.tasks.forEach((taskData) => {
        ++pendings;
        Task.load(taskData.fileName, taskData.requireName, (err, task, dependencyNames) => {
          if(err) {
            if(0 === --pendings) {
              done();
              cb();
            }
            return Log.log(err);
          }
          dependencyNameMap.set(task.name, dependencyNames);
          this.addTask(task);
          if(0 === --pendings) {
            done();
            cb();
          }
        });
      });
    });
  }
  
  save() {
    const workspace = {
      name: this.name,
      silent: this.silent,
      builds: [],
      tasks: []
    };
    this.builds.forEach((build) => {
      workspace.builds.push({
        name: build.name,
        fileName: build.fileName,
        requireName: build.requireName,
        repo: build.repoName
      });
      build.save();
    });
    this.tasks.forEach((task) => {
      workspace.tasks.push({
        name: task.name,
        fileName: task.fileName,
        requireName: task.requireName
      });
      task.save();
    });
    Fs.writeFile(`${basePath}workspace${Path.sep}${this.name}.wrk`, JSON.stringify(workspace, null, 2), (err) => {
      if(err) {
        Log.log(`workspace.save: '${basePath}workspace${Path.sep}${this.name}.wrk' ERROR:`, err);
      }
    });
  }
  
  addBuild(build) {
    this.builds.push(build);
    return build;
  }
  
  getBuild(name) {
    return this.builds.find(build => build.name === name);
  }
  
  addTask(task) {
    this.tasks.push(task);
  }
  
  init() {
    const developmentTaskName = 'task:env-development';
    const productionTaskName = 'task:env-production';
    Tr.task(developmentTaskName, (cb) => {
    //  process.env.NODE_ENV = Reflect.get(global, 'release-data@abstractor').env;
    //  Log.log(process.env.NODE_ENV);
      process.nextTick(() => {
        cb();
      });
    });
    Tr.task(productionTaskName, (cb) => {
    //  process.env.NODE_ENV = Reflect.get(global, 'release-data@abstractor').env;
    //  Log.log(process.env.NODE_ENV);
      process.nextTick(() => {
        cb();
      });
    });
    this.tasks.forEach((task) => {
      task.init();
    });
    const startBuilds = [];
    const watches = [];
    this.builds.forEach((build) => {
      build.init(this.silent);
      watches.push(build.getTaskWatch());
      if(0 === build.nbrOfChildren) {
        startBuilds.push(build);
      }
    });
    const startTasks = [];
    startBuilds.forEach((startBuild) => {
      startTasks.push(startBuild.getTaskChain());
    });
    const developmentTasks = [Tr.serial(developmentTaskName, Tr.parallel(...startTasks))];
    const productionTasks = [Tr.serial(productionTaskName, Tr.parallel(...startTasks))];
    if(!this.build) {
      developmentTasks.push(Tr.parallel(...watches));
      productionTasks.push(Tr.parallel(...watches));
    }
    Tr.task(`${developmentTaskName}:solution`, (cb) => {
      const name = `${this.name} ${this.task}${this.build ? ' --build' : ''}${this.dev ? ' --dev' : ''} development${this.bottleneck ? ' --bottleneck ' + this.bottleneck : ''}`;
      const startDate = new Date();
      Log.start(name, startDate);
      Tr.serial(...developmentTasks)(() => {
        const stopDate = new Date();
        Log.end(name, startDate, stopDate);
        Build.initialBuild = false;
        cb();
      });
    });
    Tr.task(`${productionTaskName}:solution`, (cb) => {
      const name = `${this.name} ${this.task}${this.build ? ' --build' : ''}${this.dev ? ' --dev' : ''} production${this.bottleneck ? ' --bottleneck ' + this.bottleneck : ''}`;
      const startDate = new Date();
      Log.start(name, startDate);
      Tr.serial(...productionTasks)(() => {
        const stopDate = new Date();
        Log.end(name, startDate, stopDate);
        Build.initialBuild = false;
        cb();
      });
    });
    Tr.task(`task:watch`, (cb) => {
      const name = `${this.name} ${this.task}${this.build ? ' --build' : ''}${this.dev ? ' --dev' : ''} ${Reflect.get(global, 'release-data@abstractor').env}${this.bottleneck ? ' --bottleneck ' + this.bottleneck : ''}`;
      const startDate = new Date();
      Log.start(name, startDate);
      Tr.parallel(...watches)(() => {
        const stopDate = new Date();
        Log.end(name, startDate, stopDate);
        cb();
      });
    });
  }
}


module.exports = Workspace;
