
'use strict';

const Log = require('./log');
const dynamicConfig = Reflect.get(global, 'dynamicConfig@abstractor');
const Clc = require('cli-color');


class GitGlobal {
  constructor() {
    this.taskName = '';
    this.result = null;
    this.ready = false;
    this.all = dynamicConfig.parameters.has('all');
    this.repo = dynamicConfig.parameters.get('repo');
    this.initiated = false;
  }
  
  init(taskName, all) {
    this.originalTask = taskName;
    this.result = null;
    this.ready = false;
    this.all = undefined === all ? this.all : all;
    this.initiated = true;
  }
  
  getTaskName(currentTaskName) {
    if(this.initiated) {
      return this.originalTask;
    }
    else {
      return currentTaskName;
    }
  }
  
  getResult() {
    if(this.initiated) {
      return this.result;
    }
    else {
      Log.log(Clc.red('Error:'), 'GitGlobal is not initiated.');
    }
  }
  
  setResult(result) {
    if(this.initiated) {
      this.result = result;
    }
  }
  
  getReady() {
    if(this.initiated) {
      return this.ready;
    }
    else {
      Log.log(Clc.red('Error:'), 'GitGlobal is not initiated.');
    }
  }
  
  setReady(ready) {
    if(this.initiated) {
      this.ready = ready;
    }
  }
  
  getAll() {
    return this.all;
  }
  
  getRepo() {
    return this.repo;
  }
}

module.exports = GitGlobal;
