
'use strict';

const Log = require('./log');
const TaskRunnerGlob = require('./task-runner-glob');
const Clc = require('cli-color');
const Fs = require('fs');
const Path = require('path');
const Zlib = require('zlib');


class ChildClientRcBundle {
  constructor(appName, name, silent, bundleName, repo, source, dest, remove, nodeEnv, buildEnv) {
    this.appName = appName;
    this.name = name;
    this.silent = silent;
    this.bundleName = bundleName;
    this.repo = repo;
    this.source = source;
    this.dest = dest;
    this.remove = remove;
    process.env.NODE_ENV = nodeEnv;
    this.production = 1 === buildEnv;
  }
  
  task(cb) {
    const resolvedPath = Path.resolve(this.repo ? Path.resolve(this.repo) : '.');
    const buffers = [];
    TaskRunnerGlob.glob(this.source, this.dest, resolvedPath, () => {
      const dstFile = `${this.dest}${Path.sep}${this.bundleName}`;
      if(this.production) {
        const buffer = buffers.join('');
        Zlib.gzip(buffer, (err, data) =>  {
          if(err) {
            Log.log(`Bundle ${this.name} error:`, err);
            return;
          }
          Fs.writeFile(dstFile + '.gzip', data, (err) => {
            if(err) {
              Log.log(Clc.red(`Could not write: '`) + dstFile + Clc.red(`'.`), err);
            }
          });
        });
      }
      else {
        const writeStream = Fs.createWriteStream(dstFile, {flags: 'w'});
        buffers.forEach((buffer) => {
          if(buffer) {
            writeStream.write(buffer);
          }
        });
      }
    }, (srcFile, dstFile, cb) => {
      Fs.readFile(srcFile, (err, data) => {
        if(err) {
          Log.log(Clc.red(`Could not read: '`) + srcFile + Clc.red(`'.`), err);
        }
        else {
          if(this.production) {
            const formattedData = data.toString()
              .replace(/([^0-9a-zA-Z\.\%\-#])\s+/g, "$1")
              .replace(/\s([^0-9a-zA-Z\.\%\-#]+)/g, "$1")
              .replace(/;}/g, "}")
              .replace(/\/\*.*?\*\//g, "");
            buffers.push(this._removeText(formattedData, this.remove));
          }
          else if(this.remove) {
            buffers.push(this._removeText(data.toString(), this.remove));
          }
          else {
            buffers.push(data);
          }
        }
        cb();
      });
    });
  }
  
  _removeText(text, remove) {
    if(Array.isArray(remove)) {
      for(let i = 0; i < this.remove.length; ++i) {
        text = this._removeText(text, this.remove[i]);
      }
    }
    else {
      text = text.replace(remove, '');
    }
    return text
  }
}


process.nextTick(() => {
  const parameters = JSON.parse(process.argv[2]);
  const childClientRcBundle = new ChildClientRcBundle(...parameters);
  childClientRcBundle.task(() => {
    process.send(0);
  });
});
