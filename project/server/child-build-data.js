
'use strict';

const Log = require('./log');
const TaskRunnerGlob = require('./task-runner-glob');
const Clc = require('cli-color');
const Fs = require('fs');


class ChildBuildData {
  static getBuildData(buildDataPath, cb) {
    TaskRunnerGlob.verifyPath(buildDataPath, (err) => {
      if(err) {
        if(0 === --pendings.val) {
          cb();
        }
      }
      else {
        Fs.readFile(buildDataPath, (err, data) => {
          if(!err) {
            try {
              cb({
                changed: false,
                map: new Map(JSON.parse(data))
              });
              return;
            }
            catch(e) {}
          }
          cb({
            changed: false,
            map: new Map()
          });
        });
      }
    });
  }
  
  static setBuildData(buildDataPath, buildData, cb) {
    if(buildData.changed) {
      const buldDataString = JSON.stringify(buildData.map, (key, value) => {
        if(value instanceof Map) {
          const map = [];
          value.forEach((val, k) => {
            map.push([k, val]);
          });
          return map;
        }
        else {
          return value;
        }
      }, 2);
      Fs.writeFile(buildDataPath, buldDataString, (err) => {
        if(err) {
          Log.log(Clc.red(`Could not write: '`) + buildDataPath + Clc.red(`'.`), err);
        }
        cb();
      });
    }
    else {
      process.nextTick(cb);
    }
  }
  
  static set(buildData, srcFile, mtimeMs) {
    buildData.changed = true;
    buildData.map.set(srcFile, mtimeMs);
  }
  
  static write(srcFile, dstFile, data, buildData, cb) {
    Fs.writeFile(dstFile, data, (err) => {
      if(err) {
        Log.log(Clc.red(`Could not write: '`) + dstFile + Clc.red(`'.`), err);
        cb(err);
      }
      else {
        Fs.lstat(srcFile, (err, stat) => {
          ChildBuildData.set(buildData, srcFile, stat.mtimeMs);
          cb(null);
        });
      }
    });
  }
}


module.exports = ChildBuildData;
