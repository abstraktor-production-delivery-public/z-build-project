
'use strict';

const ChildBuildData = require('./child-build-data');
const Log = require('./log');
const TaskRunnerGlob = require('./task-runner-glob');
const Clc = require('cli-color');
const Fs = require('fs');
const Os = require('os');
const Path = require('path');


class ChildClientJs {
  static SEARCH_requireAsBundle = Buffer.from('requireAsBundle');
  static SEARCH_start = 'requireAsBundle'.length + 2;
  static SEARCH_stop = Buffer.from('\')');
  static SEARCH_empty_string = Buffer.from('\'\'');
  static SEARCH_template_litteral = Buffer.from('`');

  static BUILD_RELEASE_START = '#BUILD_RELEASE_START';
  static BUILD_RELEASE_START_COMMENT_LINE = '// #BUILD_RELEASE_START';
  static BUILD_RELEASE_START_COMMENT_ALL = '/* #BUILD_RELEASE_START';
  static BUILD_RELEASE_STOP = '#BUILD_RELEASE_STOP';
  static BUILD_RELEASE_STOP_COMMENT_LINE = '// #BUILD_RELEASE_STOP';
  static BUILD_RELEASE_STOP_COMMENT_ALL = '#BUILD_RELEASE_STOP */';
  static BUILD_DEBUG_START = '#BUILD_DEBUG_START';
  static BUILD_DEBUG_START_COMMENT_LINE = '// #BUILD_DEBUG_START';
  static BUILD_DEBUG_START_COMMENT_ALL = '/* #BUILD_DEBUG_START';
  static BUILD_DEBUG_STOP = '#BUILD_DEBUG_STOP';
  static BUILD_DEBUG_STOP_COMMENT_LINE = '// #BUILD_DEBUG_STOP';
  static BUILD_DEBUG_STOP_COMMENT_ALL = '#BUILD_DEBUG_STOP */';
  
  constructor(appName, name, silent, repo, source, dest, nodeEnv) {
    this.appName = appName;
    this.name = name;
    this.silent = silent;
    this.repo = repo;
    this.source = source;
    this.dest = dest;
    this.isDebug = 'development' === nodeEnv;
    process.env.NODE_ENV = nodeEnv;
  }
    
  task(cb) {
    const pendings = {
      val: 0
    };
    const resolvedPath = Path.resolve(this.repo ? Path.resolve(this.repo) : '.');
    const buildDataPath = Path.resolve(`..${Path.sep}Generated${Path.sep}Build${Path.sep}${this.appName}-${this.name.replaceAll(':', '-').replaceAll('/', '-')}`);
    ChildBuildData.getBuildData(buildDataPath, (buildData) => {
      TaskRunnerGlob.glob(this.source, this.dest, resolvedPath, cb, (srcFile, dstFile, cb) => {
        let shallBuild = true;
        ++pendings.val;
        Fs.lstat(dstFile, (err, stat) => {
          if(shallBuild) {
            if(!!err && 'ENOENT' === err.code) {
              shallBuild = false;
              this._build(srcFile, dstFile, resolvedPath, buildDataPath, buildData, pendings, cb);
            }
            else {
              if(0 === --pendings.val) {
                ChildBuildData.setBuildData(buildDataPath, buildData, cb);
              }
            }
          }
          else {
            if(0 === --pendings.val) {
              ChildBuildData.setBuildData(buildDataPath, buildData, cb);
            }
          }
        });
        ++pendings.val;
        Fs.lstat(srcFile, (err, stat) => {
          if(shallBuild) {
            const mTimeMs = buildData.map.get(srcFile);
            if(mTimeMs === stat.mtimeMs) {
              if(0 === --pendings.val) {
                ChildBuildData.setBuildData(buildDataPath, buildData, cb);
              }
            }
            else {
              shallBuild = false;
              this._build(srcFile, dstFile, resolvedPath, buildDataPath, buildData, pendings, cb);
            }
          }
          else {
            if(0 === --pendings.val) {
              ChildBuildData.setBuildData(buildDataPath, buildData, cb);
            }
          }
        });
      });
    });
  }
  
  _build(srcFile, dstFile, resolvedPath, buildDataPath, buildData, pendings, cb) {
    Fs.readFile(srcFile, (err, data) => {
      if(err) {
        Log.log(Clc.red(`Could not read: '`) + srcFile + Clc.red(`'.`), err);
        if(0 === --pendings.val) {
          cb();
        }
      }
      else {
        TaskRunnerGlob.verifyPath(dstFile, (err) => {
          if(err) {
            if(0 === --pendings.val) {
              cb();
            }
          }
          else {
            const bundleIndex = data.indexOf(ChildClientJs.SEARCH_requireAsBundle);
            if(-1 !== bundleIndex) {
              ChildBuildData.write(srcFile, dstFile, data.subarray(0, bundleIndex), buildData, (err) => {
                if(!err) {
                  const buffers = [];
                  this._bundle(buffers, data, bundleIndex, srcFile, dstFile, resolvedPath, () => {
                    this._appendFile(dstFile, buffers, (err) => {
                      if(err) {
                        Log.log(Clc.red(`Could not write: '`) + dstFile + Clc.red(`'.`), err);
                        if(0 === --pendings.val) {
                          cb();
                        }
                      }
                      if(0 === --pendings.val) {
                        ChildBuildData.setBuildData(buildDataPath, buildData, cb);
                      }
                    });
                  });
                }
                else {
                  if(0 === --pendings.val) {
                    ChildBuildData.setBuildData(buildDataPath, buildData, cb);
                  }
                }
              });
            }
            else {
              data = this._macro(data);
              ChildBuildData.write(srcFile, dstFile, data, buildData, () => {
                if(0 === --pendings.val) {
                  ChildBuildData.setBuildData(buildDataPath, buildData, cb);
                }
              });
            }
          }
        });
      }
    });
  }
  
  _appendFile(dstFile, buffers, cb) {
    const buffer = buffers.shift();
    if(!buffer) {
      return cb();
    }
    Fs.appendFile(dstFile, buffer, (err) => {
      if(err) {
        Log.log(Clc.red(`Could not append: '`) + dstFile + Clc.red(`'.`), err);
        cb(err);
      }
      else {
        this._appendFile(dstFile, buffers, cb);
      }
    });
  }
  
  _bundle(buffers, data, bundleIndex, srcFile, dstFile, resolvedPath, cb) {
    const stopIndex = data.indexOf(ChildClientJs.SEARCH_stop, bundleIndex);
    const bundleInculde = data.subarray(bundleIndex + ChildClientJs.SEARCH_start, stopIndex).toString().replaceAll('/', Path.sep);
    let file = null;
    let srcBase = null;
    if(bundleInculde.startsWith('.')) {
      const lastIndex = srcFile.lastIndexOf(Path.sep);
      srcBase = srcFile.substring(0, lastIndex);
      file = Path.normalize(srcBase + Path.sep + bundleInculde) + '.js';
    }
    else {
      const lastIndex = resolvedPath.lastIndexOf(Path.sep);
      srcBase = resolvedPath.substring(0, lastIndex);
      const firstIndex = bundleInculde.indexOf(Path.sep);
      file = Path.normalize(srcBase + Path.sep + bundleInculde.substring(0, firstIndex) + Path.sep + 'project' + Path.sep + bundleInculde.substring(firstIndex)) + '.js';
    }
    Fs.readFile(file, (err, src) => {
      if(!err) {
        buffers.push(ChildClientJs.SEARCH_template_litteral);
        let replacedSrc = src.toString().replace('`', '\`');
        this._removeLine(buffers, replacedSrc, ['\'use strict\';', 'module.exports', '= require(\''], 0);
        buffers.push(ChildClientJs.SEARCH_template_litteral);
      }
      else {
        Log.log(err);
        buffers.push(ChildClientJs.SEARCH_empty_string);
      }
      const nextBundleIndex = data.indexOf(ChildClientJs.SEARCH_requireAsBundle, stopIndex + 2);
      if(-1 === nextBundleIndex) {
        buffers.push(data.subarray(stopIndex + 2));
        process.nextTick(cb);
      }
      else {
        buffers.push(data.subarray(stopIndex + 2, nextBundleIndex));
        this._bundle(buffers, data, nextBundleIndex, srcFile, dstFile, resolvedPath, cb);
      }
    });
  }
  
  _removeLine(buffers, src, texts, index) {
    let foundIndex = -1;
    for(let i = 0; i < texts.length; ++i) {
      const fIndex = src.indexOf(texts[i], index);
      if(-1 !== fIndex) {
        if(-1 === foundIndex) {
          foundIndex = fIndex;
        }
        else if(fIndex < foundIndex) {
          foundIndex = fIndex;
        }
      }
    }
    if(-1 === foundIndex) {
      if(0 === index) {
        buffers.push(Buffer.from(src));
      }
      else {
        buffers.push(Buffer.from(src.substring(index)));
      }
      return;
    }
    else {
      const startIndex = src.lastIndexOf(Os.EOL, foundIndex);
      const endIndex = src.indexOf(Os.EOL, foundIndex);
      if(-1 !== endIndex) {
        buffers.push(Buffer.from(src.substring(index, startIndex)));
        this._removeLine(buffers, src, texts, endIndex);
      }
    }
  }

  _macro(data) {
    const mightHaveMacro = data.indexOf('#');
    if(-1 !== mightHaveMacro) {
      if('string' !== typeof data) {
        data = data.toString();
      }
      if(this.isDebug) {
        data = data.replaceAll(ChildClientJs.BUILD_DEBUG_START, ChildClientJs.BUILD_DEBUG_START_COMMENT_LINE);
        data = data.replaceAll(ChildClientJs.BUILD_DEBUG_STOP, ChildClientJs.BUILD_DEBUG_STOP_COMMENT_LINE);
        data = data.replaceAll(ChildClientJs.BUILD_RELEASE_START, ChildClientJs.BUILD_RELEASE_START_COMMENT_ALL);
        data = data.replaceAll(ChildClientJs.BUILD_RELEASE_STOP, ChildClientJs.BUILD_RELEASE_STOP_COMMENT_ALL);
      }
      else {
        data = data.replaceAll(ChildClientJs.BUILD_RELEASE_START, ChildClientJs.BUILD_RELEASE_START_COMMENT_LINE);
        data = data.replaceAll(ChildClientJs.BUILD_RELEASE_STOP, ChildClientJs.BUILD_RELEASE_STOP_COMMENT_LINE);
        data = data.replaceAll(ChildClientJs.BUILD_DEBUG_START, ChildClientJs.BUILD_DEBUG_START_COMMENT_ALL);
        data = data.replaceAll(ChildClientJs.BUILD_DEBUG_STOP, ChildClientJs.BUILD_DEBUG_STOP_COMMENT_ALL);
      }
    }
    return data;
  }
}


process.nextTick(() => {
  const parameters = JSON.parse(process.argv[2]);
  const childClientJs = new ChildClientJs(...parameters);
  childClientJs.task(() => {
    process.send(0);
  });
});
