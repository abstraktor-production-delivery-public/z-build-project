
'use strict';

const Log = require('./log');
const Clc = require('cli-color');
const Fs = require('fs');
const Path = require('path');


class Project {
  constructor(name, fileName, source) {
  }
  
  static parseFile(data, fileName) {
    try {
      const dataObject = JSON.parse(data);
      return dataObject;
    } 
    catch(e) {
      Log.log(Clc.red('file:'), fileName, Clc.red('- JSON.parse error:'), e);
    }
  }
  
  static getPath(project, type, isPlugin, dev, organization, cb) {
    const types = type.split(',');
    let pendings = types.length;
    const results = [];
    types.forEach((type) => {
      type = type.trim();
      let repo = '';
      let devRepoExists = false;
      let nodeModulesRepoExists = false;
      if(dev) {
        repo = `..${Path.sep}${project}${Path.sep}project${Path.sep}${type}`;
      }
      else {
        if(!isPlugin) {
          const organizationPath = organization ? `${Path.sep}${organization}` : '';
          repo = `.${Path.sep}node_modules${organizationPath}${Path.sep}${project}${Path.sep}project${Path.sep}${type}`;
        }
        else {
          repo = `..${Path.sep}..${Path.sep}..${Path.sep}..${Path.sep}${project}${Path.sep}project${Path.sep}${type}`;
        }
      }
      Fs.lstat(repo, (err, stat) => {
        if(dev) {
          devRepoExists = err ? false : stat.isDirectory();
        }
        else {
          nodeModulesRepoExists = err ? false : stat.isDirectory();
        }
        results.push({error: null, buildData: null, path: repo, devRepoExists, nodeModulesRepoExists});
        if(0 === --pendings) {
          cb(results);
        }
      });
    });
  }
  
  static load(projectData, type, dev, organization, cb) {
    Project.getPath(projectData.name, type, !!projectData.plugin, dev, organization, (results) => {
      if(0 === results.length) {
        cb([{error: new Error(Clc.yellow(projectData.name + ', not found')), buildData: null, path: repo, devRepoExists, nodeModulesRepoExists}]);
        return;
      }
      let pendings = results.length;
      results.forEach((result) => {
        if(result.path) {
          const build = projectData.new ? `${Path.sep}_build` : '';
          const fileName = `${result.path}${build}${Path.sep}${projectData.name}.prj`;
          const currentResult = result;
          Fs.readFile(fileName, (err, data) => {
            if(err) {
              currentResult.error = err;
            }
            else {
              const buildData = Project.parseFile(data, fileName);
              currentResult.buildData = buildData;
            }
            if(0 === --pendings) {
              cb(results);
            }  
          });
        }
        else {
          if(projectData.optional) {
            result.error = new Error(Clc.yellow('Not found'));
          }
          else {
            result.error = new Error(Clc.red('Could not find path for project.'));
          }
          if(0 === --pendings) {
            cb(results);
          }
        }
      });
    });
  }
}


module.exports = Project;
