
'use strict';


class BottleneckQueue {
  constructor(maxExecutions) {
    this.queue = [];
    this.maxExecutions = maxExecutions ? maxExecutions : 0;
    this.ongoingExecutions = 0;
  }
  
  _execute(job) {
    job((cbExec) => {
      if(0 !== this.queue.length) {
        process.nextTick(() => {
          this._execute(this.queue.shift());
        });
      }
      else {
        --this.ongoingExecutions;
      }
    });
  }
  
  execute(job) {
    if(0 === this.maxExecutions || this.ongoingExecutions < this.maxExecutions) {
      ++this.ongoingExecutions;
      this._execute(job);
    }
    else {
      this.queue.push(job);
    }
  }
}


module.exports = BottleneckQueue;
