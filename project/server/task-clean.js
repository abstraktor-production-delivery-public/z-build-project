
'use strict';

const Task = require('./task');
const Log = require('./log');
const Clc = require('cli-color');
const Fs = require('fs');
const Path = require('path');


class TaskClean extends Task {
  constructor(part, path, reqursive) {
    super(part, __filename);
    this.part = part;
    this.paths = Array.isArray(path) ? path : [path];
    this.reqursive = reqursive;
  }
  
  saveData() {
    return {
      part: this.part,
      paths: this.paths,
      reqursive: this.reqursive
    }; 
  }
  
  task(cb) {
    let pendings = 0;
    this.paths.forEach((path) => {
      ++pendings;
      Fs.rm(path, {recursive:true, force:true}, (err) => {
        if(err) {
          Log.log(this.part, Clc.red('error:'), err);
        }
        if(0 === --pendings) {
          Log.log(this.part, Clc.green('done.'));
          cb();
        }
      });
    });
  }
}


module.exports = TaskClean;
