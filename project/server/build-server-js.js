
'use strict';

const Build = require('./build');
const Log = require('./log');
const ChildProcess = require('child_process');
const Path = require('path');
const Clc = require('cli-color');


class BuildServerJs extends Build {
  constructor(appName, layer, part, source, dest, replaceCondition, replaceHandle) {
    super(appName, `Server:${layer}Layer/${part}`, __filename, source, dest);
    this.layer = layer;
    this.part = part;
    this.replaceCondition = replaceCondition ? (new RegExp(replaceCondition)).source : null;
    this.replaceHandle = replaceHandle ? ('string' === typeof replaceHandle ? replaceHandle : replaceHandle.toString().replace(/^\W*(function[^{]+\{([\s\S]*)\}|[^=]+=>[^{]*\{([\s\S]*)\}|[^=]+=>(.+))/i, '$2$3$4')) : null;
  }
  
  saveData() {
    return {
      layer: this.layer,
      part: this.part,
      source: this.source,
      dest: this.dest,
      replaceCondition: this.replaceCondition,
      replaceHandle: this.replaceHandle
    };
  }
  
  task(cb) {
    this.execute(0, 0, cb);
  }
  
  execute(retries, code, cb) {
    if(2 >= retries) {
      const childProcess = ChildProcess.fork(require.resolve('./child-server-js.js'), [JSON.stringify([this.appName, this.name, this.silent, this.repo, this.source, this.dest, process.env.NODE_ENV, this.replaceCondition, this.replaceHandle])], {execArgv: []});
      let done = false;
      childProcess.on('exit', (code) => {
        if(!done) {
          if(0 !== code) {
            return this.execute(++retries, code, cb);
          }
          done = true;
          cb();
        }
      });
      childProcess.on('message', (code) => {
        if(!done && 0 === code) {
          done = true;
          cb();
        }
      });
    }
    else {
      Log.log(Clc.red(`${this.name} - build-server-js: code`), code);
      cb(new Error(`Failed to build ${this.name} - build-server-js.`));
    }
  }
}

module.exports = BuildServerJs;
