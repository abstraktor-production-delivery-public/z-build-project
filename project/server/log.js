
'use strict';

const Clc = require('cli-color');


class Log {
  static log = console.log.bind(console);
  static error = console.error.bind(console);
  
  static start(name, startDate) {
    Log.log('[' + Clc.blackBright(`${startDate.getHours()}`.padStart(2, '0') + ':' + `${startDate.getMinutes()}`.padStart(2, '0') + ':' + `${startDate.getSeconds()}`.padStart(2, '0')) + '] Starting ' + Clc.cyan(name) + '');
  }
  
  static end(name, startDate, stopDate) {
    const diff = stopDate - startDate;
    const milliseconds = parseInt((diff%1000));
    const seconds = parseInt((diff/1000)%60);
    const minutes = parseInt((diff/(1000*60))%60);
    const hours = parseInt((diff/(1000*60*60))%24);
    let diffResult = '';
    if(hours > 0) {
      diffResult += `${hours}h `;
      diffResult += `${minutes}min `;
      diffResult += `${seconds}s `;
      diffResult += `${milliseconds}ms`;
    }
    else if(minutes > 0) {
      diffResult += `${minutes}min `;
      diffResult += `${seconds}s `;
      diffResult += `${milliseconds}ms`;
    }
    else if(seconds > 0) {
      diffResult += `${seconds}s `;
      diffResult += `${milliseconds}ms`;
    }
    else {
      diffResult += `${milliseconds}ms`;
    }
    Log.log('[' + Clc.blackBright(`${stopDate.getHours()}`.padStart(2, '0') + ':' + `${stopDate.getMinutes()}`.padStart(2, '0') + ':' + `${stopDate.getSeconds()}`.padStart(2, '0')) + '] Finished ' + Clc.cyan(name) + ' after', Clc.magenta(`${diffResult}`));
  }
}


module.exports = Log;
