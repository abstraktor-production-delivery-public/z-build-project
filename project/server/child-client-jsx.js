
'use strict';

const ChildBuildData = require('./child-build-data');
const Log = require('./log');
const TaskRunnerGlob = require('./task-runner-glob');
const Clc = require('cli-color');
const Babel = require('@babel/core');
const Fs = require('fs');
const Path = require('path');


class ChildClientJsx {
  static BUILD_RELEASE_START = '#BUILD_RELEASE_START';
  static BUILD_RELEASE_START_COMMENT_LINE = '// #BUILD_RELEASE_START';
  static BUILD_RELEASE_START_COMMENT_ALL = '/* #BUILD_RELEASE_START';
  static BUILD_RELEASE_STOP = '#BUILD_RELEASE_STOP';
  static BUILD_RELEASE_STOP_COMMENT_LINE = '// #BUILD_RELEASE_STOP';
  static BUILD_RELEASE_STOP_COMMENT_ALL = '#BUILD_RELEASE_STOP */';
  static BUILD_DEBUG_START = '#BUILD_DEBUG_START';
  static BUILD_DEBUG_START_COMMENT_LINE = '// #BUILD_DEBUG_START';
  static BUILD_DEBUG_START_COMMENT_ALL = '/* #BUILD_DEBUG_START';
  static BUILD_DEBUG_STOP = '#BUILD_DEBUG_STOP';
  static BUILD_DEBUG_STOP_COMMENT_LINE = '// #BUILD_DEBUG_STOP';
  static BUILD_DEBUG_STOP_COMMENT_ALL = '#BUILD_DEBUG_STOP */';
  
  constructor(appName, name, silent, repo, source, dest, nodeEnv) {
    this.appName = appName;
    this.name = name;
    this.silent = silent;
    this.repo = repo;
    this.source = source;
    this.dest = dest;
    this.isDebug = 'development' === nodeEnv;
    process.env.NODE_ENV = nodeEnv;
  }
  
  task(cb) {
    const pendings = {
      val: 0
    };
    const resolvedPath = Path.resolve(this.repo ? Path.resolve(this.repo) : '.');
    const buildDataPath = Path.resolve(`..${Path.sep}Generated${Path.sep}Build${Path.sep}${this.appName}-${this.name.replaceAll(':', '-').replaceAll('/', '-')}`);
    ChildBuildData.getBuildData(buildDataPath, (buildData) => {
      TaskRunnerGlob.glob(this.source, this.dest, resolvedPath, cb, (srcFile, dstFile, cb) => {
        let shallBuild = true;
        dstFile = dstFile.substring(0, dstFile.length - 2) + 's';
        ++pendings.val;
        Fs.lstat(dstFile, (err, stat) => {
          if(shallBuild) {
            if(!!err && 'ENOENT' === err.code) {
              shallBuild = false;
              this._build(srcFile, dstFile, buildDataPath, buildData, pendings, cb);
            }
            else {
              if(0 === --pendings.val) {
                ChildBuildData.setBuildData(buildDataPath, buildData, cb);
              }
            }
          }
          else {
            if(0 === --pendings.val) {
              ChildBuildData.setBuildData(buildDataPath, buildData, cb);
            }
          }
        });
        ++pendings.val;
        Fs.lstat(srcFile, (err, stat) => {
          if(shallBuild) {
            const mTimeMs = buildData.map.get(srcFile);
            if(mTimeMs === stat.mtimeMs) {
              if(0 === --pendings.val) {
                ChildBuildData.setBuildData(buildDataPath, buildData, cb);
              }
            }
            else {
              shallBuild = false;
              this._build(srcFile, dstFile, buildDataPath, buildData, pendings, cb);
            }
          }
          else {
            if(0 === --pendings.val) {
              ChildBuildData.setBuildData(buildDataPath, buildData, cb);
            }
          }
        });
      });
    });
  }
  
  _build(srcFile, dstFile, buildDataPath, buildData, pendings, cb) {
    Fs.readFile(srcFile, (err, data) => {
      if(err) {
        Log.log(Clc.red(`Could not read: '`) + srcFile + Clc.red(`'.`), err);
        if(0 === --pendings.val) {
          ChildBuildData.setBuildData(buildDataPath, buildData, cb);
        }
      }
      else {
        TaskRunnerGlob.verifyPath(dstFile, (err) => {
          if(err) {
            if(0 === --pendings.val) {
              cb();
            }
          }
          else {
            data = this._macro(data);
            Babel.transform(data, {
              plugins: ["@babel/plugin-transform-react-jsx"],
            }, (err, result) => {
              if(err) {
                Log.log(Clc.red('plugin-transform-react-jsx error:'), srcFile, err);
                if(0 === --pendings.val) {
                  ChildBuildData.setBuildData(buildDataPath, buildData, cb);
                }
              }
              else {
                ChildBuildData.write(srcFile, dstFile, result.code, buildData, () => {
                  if(0 === --pendings.val) {
                    ChildBuildData.setBuildData(buildDataPath, buildData, cb);
                  }
                });
              }
            });
          }
        });
      }
    });
  }
  
  _macro(data) {
    const mightHaveMacro = data.indexOf('#');
    if(-1 !== mightHaveMacro) {
      if('string' !== typeof data) {
        data = data.toString();
      }
      if(this.isDebug) {
        data = data.replaceAll(ChildClientJsx.BUILD_DEBUG_START, ChildClientJsx.BUILD_DEBUG_START_COMMENT_LINE);
        data = data.replaceAll(ChildClientJsx.BUILD_DEBUG_STOP, ChildClientJsx.BUILD_DEBUG_STOP_COMMENT_LINE);
        data = data.replaceAll(ChildClientJsx.BUILD_RELEASE_START, ChildClientJsx.BUILD_RELEASE_START_COMMENT_ALL);
        data = data.replaceAll(ChildClientJsx.BUILD_RELEASE_STOP, ChildClientJsx.BUILD_RELEASE_STOP_COMMENT_ALL);
      }
      else {
        data = data.replaceAll(ChildClientJsx.BUILD_RELEASE_START, ChildClientJsx.BUILD_RELEASE_START_COMMENT_LINE);
        data = data.replaceAll(ChildClientJsx.BUILD_RELEASE_STOP, ChildClientJsx.BUILD_RELEASE_STOP_COMMENT_LINE);
        data = data.replaceAll(ChildClientJsx.BUILD_DEBUG_START, ChildClientJsx.BUILD_DEBUG_START_COMMENT_ALL);
        data = data.replaceAll(ChildClientJsx.BUILD_DEBUG_STOP, ChildClientJsx.BUILD_DEBUG_STOP_COMMENT_ALL);
      }
    }
    return data;
  }
}


process.nextTick(() => {
  const parameters = JSON.parse(process.argv[2]);
  const childClientJsx = new ChildClientJsx(...parameters);
  childClientJsx.task(() => {
    process.send(0);
  });
});
