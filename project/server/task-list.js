
'use strict';

const Tr = Reflect.get(global, 'tr@abstractor');
const Task = require('./task');


class TaskList extends Task {
  constructor(part, list) {
    super(part, __filename, list);
    this.part = part;
    this.list = list;
  }
  
  saveData() {
    return {
      part: this.part,
      list: this.list
    }; 
  }
  
  task(cb) {
    Tr.serial(...this.list)(cb);
  }
}

module.exports = TaskList;
