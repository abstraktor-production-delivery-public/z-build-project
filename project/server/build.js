
'use strict';

const Log = require('./log');
const Clc = require('cli-color');
const Tr = Reflect.get(global, 'tr@abstractor');
const Fs = require('fs');
const Path = require('path');


class Build {
  static runningTasks = 0;
  static initialBuild = true;

  constructor(appName, name, fileName, source, dest) {
    this.bottleneckQueue = null;
    this.appName = appName;
    this.name = name;
    this.fileName = name.replace(/[/\\?%*:|"<>]/g, '-');
    this.requireName = fileName.substring(__dirname.length + 1, fileName.length - '.js'.length);
    this.watchName = `watch:${name}`;
    this.rawSource = source;
    this.source = [];
    this.dest = dest;
    this.isBundle = false;
    this.repo = null;
    this.sourceWatch = [];
    this.silent = false;
    this.parents = [];
    this.nbrOfChildren = 0;
    this.executingChildren = 0;
    this.watchTrigger = false;
    this.haveChildrenChanged = false;
    this.initialized = false;
    this.taskChain = null;
    this.requestDate = null;
    this.startDate = null;
    this.taskQueued = false;
  }
  
  setBottleneckQueue(bottleneckQueue) {
    this.bottleneckQueue = bottleneckQueue;
  }
  
  _formatSource(organization, src) {
    return src.replace('${appName}', organization ? '../..' : this.appName);
  }
  
  addSourceFromString(organization) {
    const src = this._formatSource(organization, this.rawSource);
    this.source.push(src);
    this.sourceWatch.push(src);
  }
  
  addSourceFromObject(organization) {
    const src = this.rawSource;
    if(Array.isArray(src)) {
      
    }
    else {
      const path = this._formatSource(organization, src.path);
      src.filters.forEach((filter) => {
        this.source.push(`${path}/**/*.${filter}`);
        this.sourceWatch.push(path);
      });
    }
  } 
  
  setRepo(organization, repo, isPlugin, repoPath, local, dev) {
    if(null == repo) {
      this.repo = `.${Path.sep}`;
    }
    else if(dev) {
      this.repo =  `..${Path.sep}${repoPath}${Path.sep}${repo}`;
    }
    else if(local) {
      this.repo =  `..${Path.sep}..${Path.sep}..${Path.sep}${repoPath}${Path.sep}${repo}`;
    }
    else {
      if(!isPlugin) {
        const organizationPath = organization ? `${organization}${Path.sep}` : '';
        this.repo = `.${Path.sep}node_modules${Path.sep}${organizationPath}${repo}`;
      }
      else {
        this.repo = `..${Path.sep}..${Path.sep}..${Path.sep}..${Path.sep}${repo}`;
      }
    }
    if(Array.isArray(this.rawSource)) {
      console.log('OLD:', this.name, this.rawSource);
      this.rawSource.forEach((src) => {
        if('object' === typeof src) {
          this.source.push(this._formatSource(organization, src.path) + src.filter);
        }
        else if('string' === typeof src) {
          this.source.push(this._formatSource(organization, src));
        }
      });
    }
    else if('object' === typeof this.rawSource) {
      this.addSourceFromObject(organization);
    }
    else if('string' === typeof this.rawSource) {
      console.log('OLD:', this.name, this.rawSource);
      this.addSourceFromString(organization);
    }
  }
  
  static parseFile(data, fileName) {
    try {
      const dataObject = JSON.parse(data);
      return dataObject;
    } 
    catch(e) {
      Log.log(Clc.red('file:'), fileName, Clc.red('- JSON.parse error:'), e);
    }
  }
  
  static load(appName, fileName, requireName, bottleneckQueue, cb) {
    Fs.readFile(fileName, (err, data) => {
      if(err) {
        return cb(err);
      }
      const dataObject = Build.parseFile(data, fileName);
      const keys = Reflect.ownKeys(dataObject);
      const parameters = [];
      let dependencyNames = [];
      keys.forEach((key) => {
        if('dependencyNames' !== key) {
          if(!key.startsWith('_')) {
            parameters.push(Reflect.get(dataObject, key));
          }
        }
        else {
          dependencyNames = Reflect.get(dataObject, key);
        }
      });
      const B = require(`.${Path.sep}` + requireName);
      const build = new B(appName, ...parameters);
      build.setBottleneckQueue(bottleneckQueue);
      build.isBundle = undefined !== dataObject._bundle ? dataObject._bundle : false;
      cb(err, build, dependencyNames);
    });
  }
  
  init(silent) {
    if(!this.initialized) {
      this.silent = silent;
      Tr.task(this.name, this.executeTask.bind(this));
      Tr.task(this.watchName, this.watch.bind(this));
      this.initialized  = true;
    }
  }
  
  addDependency(dependency) {
    dependency.parents.push(this);
    ++this.nbrOfChildren;
    ++this.executingChildren;
  }
  
  printParentTree(depth) {
    Log.log(this.name.padStart(this.name.length + 2 * depth, '  '));
    ++depth;
    this.parents.forEach((parent) => {
      parent.printParentTree(depth);
    });
  }
  
  executeTask(cb) {
    if(0 === this.executingChildren && (this.watchTrigger || 0 === this.nbrOfChildren || this.haveChildrenChanged)) {
      const requestDate = new Date();
      if(null === this.startDate) {
        if(null === this.requestDate) {
          this.requestDate = requestDate;
        }
        else {
          process.nextTick(() => {
            cb();
          });
          return;
        }
      }
      this.bottleneckQueue.execute((cbExec) => {
        const startDate = this.startDate = new Date();        
        this.started(this.name, startDate);
        this.task((err) => {
          this.requestDate = null;
          this.startDate = null;
          this.stopped(this.name, startDate, new Date());
          this.changed(this.name);
          cbExec();
          cb(err);
        });
      });
    }
    else if(0 !== this.executingChildren && this.watchTrigger) {
      this.taskQueued = true;
      process.nextTick(() => {
        cb();
      });
    }
    else {
      process.nextTick(() => {
        cb();
      });
    }
  }
    
  watch(cb) {
    const resolvedPath = Path.resolve(this.repo ? Path.resolve(this.repo) : '.');
    Tr.watch(this.name, this.sourceWatch, {ignoreInitial:true, cwd: resolvedPath}, (cb) => {
      this.watchTrigger = true;
      Tr.run(this.name, cb);
    });
    process.nextTick(cb);
  }
  
  getTaskWatch() {
    return this.watchName;
  }
  
  getTaskChain() {
    if(null === this.taskChain) {
      const parentTasks = [];
      this.parents.forEach((parent) => {
        parentTasks.push(parent.getTaskChain());
      });
      if(0 !== parentTasks.length) {
        this.taskChain = Tr.serial(this.name, Tr.parallel(...parentTasks));
      }
      else {
        this.taskChain = Tr.serial(this.name);
      }
    }
    return this.taskChain;
  }
  
  started(name, startDate) {
    Log.start(name, startDate);
    ++Build.runningTasks;
    this.parents.forEach((parent) => {
      parent.childStarted(name);
    });
  }
  
  stopped(name, startDate, stopDate) {
    const diff = stopDate - startDate;
    Log.end(name, startDate, stopDate);
    this.parents.forEach((parent) => {
      parent.childStopped(name);
    });
    this.haveChildrenChanged = false;
    this.watchTrigger = false;
    if(0 === --Build.runningTasks) {
      const current = Path.resolve('.');
      Object.keys(require.cache).forEach((key) => {
        if(key.startsWith(`${current}${Path.sep}dist`)) {
          if(undefined !== require.cache[key].exports._) {
            const restore_ = require.cache[key].exports._;
            delete require.cache[key];
            const toRestore = require(key);
            toRestore._ = restore_;
          }
          else {
            delete require.cache[key];
          }
        }
      });
    }
  }
  
  changed(name) {
    this.parents.forEach((parent) => {
      parent.childChanged(name);
    });
  }
  
  childStarted(name) {
    if(!Build.initialBuild) {
      ++this.executingChildren;
    }
  }
  
  childStopped(name) {
    --this.executingChildren;
    if(this.taskQueued) {
      this.watchTrigger = true;
      this.taskQueued = false;
      if(!Build.initialBuild) {
        Tr.run(this.name);
      }
    }
  }
  
  childChanged(name) {
    this.haveChildrenChanged = true;
  }
}


module.exports = Build;
