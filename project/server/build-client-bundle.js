
'use strict';

const Build = require('./build');
const Log = require('./log');
const Clc = require('cli-color');
const ChildProcess = require('child_process');
const Fs = require('fs');


class BuildClientBundle extends Build {
  constructor(appName, layer, part, source, dest, ignores, externals, externalGlobs, exports, exportSource, shim) {
    super(appName, `Bundle:${layer}Layer/${part}`, __filename, source, dest);
    this.layer = layer;
    this.part = part;
    this.bundleName = `${layer}Layer-${part}.js`;
    this.ignores = ignores;
    this.externals = externals;
    this.externalGlobs = externalGlobs;
    this.exports = exports;
    this.exportSource = exportSource;
    this.shim = shim;
    this.buildEnv = 'development' === Reflect.get(global, 'release-data@abstractor').env ? 0 : 1;
  }
  
  saveData() {
    return {
      layer: this.layer,
      part: this.part,
      source: this.source,
      dest: this.dest,
      ignores: this.ignores,
      externals: this.externals,
      externalGlobs: this.externalGlobs,
      exports: this.exports,
      exportSource: this.exportSource,
      shim: this.shim
    };
  }
  
  task(cb) {
    Fs.access(this.dest, Fs.constants.F_OK, (err) => {
      if(!err) {
        this.execute(0, 0, cb);
      }
      else if('ENOENT' === err.code) {
        Fs.mkdir(this.dest, {recursive: true}, (err) => {
          this.execute(0, 0, cb);
        });
      }
      else {
        cb(err);
      }
    });
  }
  
  execute(retries, code, cb) {
    if(2 >= retries) {
      const childProcess = ChildProcess.fork(require.resolve('./child-client-bundle.js'), [JSON.stringify([this.appName, this.name, this.silent, this.bundleName, this.source, this.dest, this.ignores, this.externals, this.externalGlobs, this.exports, this.exportSource, this.shim, process.env.NODE_ENV, this.buildEnv])], {execArgv: []});
      let done = false;
      childProcess.on('exit', (code) => {
        if(!done) {
          if(0 !== code) {
            return this.execute(++retries, code, cb);
          }
          done = true;
          cb();
        }
      });
      childProcess.on('message', (code) => {
        if(0 === code) {
          done = true;
          cb();
        }
      });
    }
    else {
      Log.log(Clc.red(`${this.name} - build-client-bundle: code`), code);
      cb(new Error(`Failed to build ${this.name} - build-client-bundle.`));
    }
  }
}

module.exports = BuildClientBundle;
