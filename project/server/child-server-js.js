
'use strict';

const ChildBuildData = require('./child-build-data');
const TaskRunnerGlob = require('./task-runner-glob');
const Clc = require('cli-color');
const Fs = require('fs');
const Os = require('os');
const Path = require('path');


class ChildServerJs {
  static BUILD_RELEASE_START = '#BUILD_RELEASE_START';
  static BUILD_RELEASE_START_COMMENT_LINE = '// #BUILD_RELEASE_START';
  static BUILD_RELEASE_START_COMMENT_ALL = '/* #BUILD_RELEASE_START';
  static BUILD_RELEASE_STOP = '#BUILD_RELEASE_STOP';
  static BUILD_RELEASE_STOP_COMMENT_LINE = '// #BUILD_RELEASE_STOP';
  static BUILD_RELEASE_STOP_COMMENT_ALL = '#BUILD_RELEASE_STOP */';
  static BUILD_DEBUG_START = '#BUILD_DEBUG_START';
  static BUILD_DEBUG_START_COMMENT_LINE = '// #BUILD_DEBUG_START';
  static BUILD_DEBUG_START_COMMENT_ALL = '/* #BUILD_DEBUG_START';
  static BUILD_DEBUG_STOP = '#BUILD_DEBUG_STOP';
  static BUILD_DEBUG_STOP_COMMENT_LINE = '// #BUILD_DEBUG_STOP';
  static BUILD_DEBUG_STOP_COMMENT_ALL = '#BUILD_DEBUG_STOP */';
  
  constructor(appName, name, silent, repo, source, dest, nodeEnv, replaceCondition, replaceHandle) {
    this.appName = appName;
    this.name = name;
    this.silent = silent;
    this.repo = repo;
    this.source = source;
    this.dest = dest;
    this.replaceCondition = null !== replaceCondition ? new RegExp(replaceCondition, 'g') : null;
    this.replaceHandle = null !== replaceHandle ? new Function('match', 'parameters', replaceHandle) : null;
    this.isDebug = 'development' === nodeEnv;
    process.env.NODE_ENV = nodeEnv;
  }
    
  task(cb) {
    const pendings = {
      val: 0
    };
    const buildDataPath = Path.resolve(`..${Path.sep}Generated${Path.sep}Build${Path.sep}${this.appName}-${this.name.replaceAll(':', '-').replaceAll('/', '-')}`);
    const resolvedPath = Path.resolve(this.repo ? Path.resolve(this.repo) : '.');
    ChildBuildData.getBuildData(buildDataPath, (buildData) => {
      TaskRunnerGlob.glob(this.source, this.dest, resolvedPath, cb, (srcFile, dstFile, cb) => {
        let shallBuild = true;
        ++pendings.val;
        Fs.lstat(dstFile, (err, stat) => {
          if(shallBuild) {
            if(!!err && 'ENOENT' === err.code) {
              shallBuild = false;
              this._build(srcFile, dstFile, buildDataPath, buildData, pendings, cb);
            }
            else {
              if(0 === --pendings.val) {
                ChildBuildData.setBuildData(buildDataPath, buildData, cb);
              }
            }
          }
          else {
            if(0 === --pendings.val) {
              ChildBuildData.setBuildData(buildDataPath, buildData, cb);
            }
          }
        });
        ++pendings.val;
        Fs.lstat(srcFile, (err, stat) => {
          if(shallBuild) {
            const mTimeMs = buildData.map.get(srcFile);
            if(mTimeMs === stat.mtimeMs) {
              if(0 === --pendings.val) {
                ChildBuildData.setBuildData(buildDataPath, buildData, cb);
              }
            }
            else {
              shallBuild = false;
              this._build(srcFile, dstFile, buildDataPath, buildData, pendings, cb);
            }
          }
          else {
            if(0 === --pendings.val) {
              ChildBuildData.setBuildData(buildDataPath, buildData, cb);
            }
          }
        });
      });
    });
  }
  
  _build(srcFile, dstFile, buildDataPath, buildData, pendings, cb) {
    Fs.readFile(srcFile, (err, data) => {
      if(err) {
        if(0 === --pendings.val) {
          process.nextTick(cb, err);
        }
      }
      else {
        TaskRunnerGlob.verifyPath(dstFile, (err) => {
          if(err) {
            if(0 === --pendings.val) {
              process.nextTick(cb, err);
            }
          }
          else {
            data = this._macro(data);
            if(this.replaceHandle) {
              if('string' !== typeof data) {
                data = data.toString();
              }
              data = data.replace(this.replaceCondition, (...parameters) => {
                const match = parameters.shift();
                return this.replaceHandle(match, parameters);
              });
            }
            ChildBuildData.write(srcFile, dstFile, data, buildData, () => {
              if(0 === --pendings.val) {
                ChildBuildData.setBuildData(buildDataPath, buildData, cb);
              }
            });
          }
        });
      }
    });
  }

  _macro(data) {
    const mightHaveMacro = data.indexOf('#');
    if(-1 !== mightHaveMacro) {
      if('string' !== typeof data) {
        data = data.toString();
      }
      if(this.isDebug) {
        data = data.replaceAll(ChildServerJs.BUILD_DEBUG_START, ChildServerJs.BUILD_DEBUG_START_COMMENT_LINE);
        data = data.replaceAll(ChildServerJs.BUILD_DEBUG_STOP, ChildServerJs.BUILD_DEBUG_STOP_COMMENT_LINE);
        data = data.replaceAll(ChildServerJs.BUILD_RELEASE_START, ChildServerJs.BUILD_RELEASE_START_COMMENT_ALL);
        data = data.replaceAll(ChildServerJs.BUILD_RELEASE_STOP, ChildServerJs.BUILD_RELEASE_STOP_COMMENT_ALL);
      }
      else {
        data = data.replaceAll(ChildServerJs.BUILD_RELEASE_START, ChildServerJs.BUILD_RELEASE_START_COMMENT_LINE);
        data = data.replaceAll(ChildServerJs.BUILD_RELEASE_STOP, ChildServerJs.BUILD_RELEASE_STOP_COMMENT_LINE);
        data = data.replaceAll(ChildServerJs.BUILD_DEBUG_START, ChildServerJs.BUILD_DEBUG_START_COMMENT_ALL);
        data = data.replaceAll(ChildServerJs.BUILD_DEBUG_STOP, ChildServerJs.BUILD_DEBUG_STOP_COMMENT_ALL);
      }
    }
    return data;
  }
}


process.nextTick(() => {
  const parameters = JSON.parse(process.argv[2]);
  const childServerJs = new ChildServerJs(...parameters);
  childServerJs.task(() => {
    process.send(0);
  });
});
