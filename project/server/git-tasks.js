
'use strict';

const GitGlobal = require('./git-global');
const Log = require('./log');
const RepoManager = require('./repo-manager');
const Tr = Reflect.get(global, 'tr@abstractor');
const GitSimple = require('simple-git');
const Clc = require('cli-color');
const Fs = require('fs');
const Path = require('path');
const gitGlobal = new GitGlobal();
const Settings = tryRequire('z-build-build/project/server/tasks/settings');
const Generate = tryRequire('z-build-build/project/server/tasks/generate');
const Development = tryRequire('z-build-build/project/server/tasks/development');
const Candidate = tryRequire('z-build-build/project/server/tasks/candidate');
const Delivery = tryRequire('z-build-build/project/server/tasks/delivery');
const _RepoManager = new RepoManager(!!Generate);

if(Generate) {
  Settings.init(_RepoManager);
  Generate.init(_RepoManager);
  Development.init(_RepoManager);
  Candidate.init(_RepoManager);
  Delivery.init(_RepoManager);
}

function printStatus(repo) {
  const result = repo.result;
  if(null === result) {
    Log.log('  ' + repo.name + (repo.error ? `, error: ${repo.error}` : '') + (repo.isRepo ? '' : Clc.blue(' - is not a repo yet')));
  }
  else {
    Log.log('  ' + repo.name + (repo.error ? `, error: ${repo.error}` : '') + (repo.isRepo ? ' - On branch ' + Clc.cyan(result.current) : ' - is not a repo yet.'));
    if(0 !== result.ahead) {
      Log.log(`    Your branch is ahead of '${result.tracking}' by ` + Clc.magenta(`${result.ahead}`) + ` commit${1 === result.ahead ? '' : 's'}.`);
    }
    if(0 !== result.behind) {
      Log.log(`    Your branch is behind of '${result.tracking}' by ` + Clc.magenta(`${result.behind}`) + ` commit${1 === result.behind ? '' : 's'}.`);
    }
    if(0 !== result.staged.length) {
      Log.log('    Changes to be committed:');
      result.staged.forEach((staged) => {
        let status = '';
        if(undefined !== result.modified.find(element => element === staged)) {
          status = 'modified';
        }
        else if(undefined !== result.deleted.find(element => element === staged)) {
          status = 'deleted';
        }
        else if(undefined !== result.renamed.find(element => element === staged)) {
          status = 'renamed';
        }
        Log.log(Clc.green(`      ${status}: ${staged}`));
      });
    }
    if(0 !== result.modified.length || 0 !== result.deleted.length || 0 !== result.renamed.length) {
      const modifiedNotStaged = [];
      const deletedNotStaged = [];
      const renamedNotStaged = [];
      result.modified.forEach((modified) => {
        if(undefined === result.staged.find(element => element === modified)) {
          modifiedNotStaged.push(modified);
        }
      });
      result.deleted.forEach((deleted) => {
        if(undefined === result.staged.find(element => element === deleted)) {
          deletedNotStaged.push(deleted);
        }
      });
      result.renamed.forEach((renamed) => {
        if(undefined === result.staged.find(element => element === renamed)) {
          renamedNotStaged.push(renamed);
        }
      });
      if(0 !== modifiedNotStaged.length || 0 !== deletedNotStaged.length || 0 !== renamedNotStaged.length) {
        Log.log('    Changes not staged for commit:');
        result.modified.forEach((modified) => {
          Log.log(Clc.red(`      modified: ${modified}`));
        });
        result.deleted.forEach((deleted) => {
          Log.log(Clc.red(`      deleted: ${deleted}`));
        });
        result.renamed.forEach((renamed) => {
           Log.log(Clc.red(`      renamed: ${renamed}`));
        });
      }
    }
    if(0 !== result.not_added.length) {
      Log.log('    Untracked files:');
      result.not_added.forEach((not_added) => {
        Log.log(Clc.red(`      ${not_added}`));
      });
    }
    if(0 !== result.conflicted.length) {
      Log.log('    Untmerged paths:');
      result.conflicted.forEach((conflicted) => {
        Log.log(Clc.red(`      ${conflicted}`));
      });
    }
  }
}

function printRepoStatus(repoGroups, heading = true) {
  if(heading) {
    const headingText = `*** ${Reflect.get(global, 'dynamicConfig@abstractor').app} status ***`;
    const line = '*'.repeat(headingText.length);
    Log.log(Clc.yellow(line));
    Log.log(Clc.yellow(headingText));
    Log.log(Clc.yellow(line));
  }
  repoGroups.forEach((repoGroup) => {
    Log.log(Clc.yellow(repoGroup.name + ' Repos'));
    repoGroup.repos.forEach((repo) => {
      printStatus(repo);
    });
  });
}

function printCloneResult(repoResults, cloneRepoResult, cloneRepo) {
  const globalTask = gitGlobal.getTaskName('Clone');
  let repoFound = false;
  let printText = ''  
  let cloneResult = true;
  repoResults.repoGroups.forEach((repoGroup) => {
    if(null === repoGroup.result) {
      return;
    }
    if(0 === repoGroup.result.repoSucceded.length && 0 === repoGroup.result.repoFailed.length && 0 === repoGroup.result.repoExists.length && 0 === repoGroup.result.folderExists.length) {
      return;
    }
    else {
      repoFound = true;
      printText += Clc.yellow(repoGroup.name, 'Repos') + '\r\n';
    }
    if(0 !== repoGroup.result.repoSucceded.length) {
      printText += '  Cloned repos:'+ '\r\n';
      repoGroup.result.repoSucceded.forEach((repo) => {
        printText += Clc.green(`    ${repo.name}`) + '\r\n';
      });
    }
    if(0 !== repoGroup.result.repoFailed.length) {
      printText += '  Failed to clone repos:'+ '\r\n';
      repoGroup.result.repoFailed.forEach((repo) => {
        printText += Clc.red(`    ${repo.name}`) + '\r\n';
      });
      cloneResult = false;
    }
    if(0 !== repoGroup.result.repoExists.length) {
      printText += '  No clone when repo already exists, repos:'+ '\r\n';
      repoGroup.result.repoExists.forEach((repo) => {
        printText += Clc.green(`    ${repo.name}`) + '\r\n';
      });
    }
    if(0 !== repoGroup.result.folderExists.length) {
      printText += '  Can not clone to existing folders, repos:'+ '\r\n';
      repoGroup.result.folderExists.forEach((repo) => {
        printText += Clc.red(`    ${repo.name}`) + '\r\n';
      });
      cloneResult = false;
    }
  });
  if(cloneRepo && !repoFound) {
    printText += `The repo '` + Clc.red(cloneRepo) + `' could not be found in the configuration.`;
    cloneResult = false;
  }
  if('Clone' === globalTask) {
    const headingText = '*** clone result ***';
    const line = '*'.repeat(headingText.length);
    Log.log(Clc.yellow(line));
    Log.log(Clc.yellow(headingText));
    Log.log(Clc.yellow(line));
    Log.log(printText);
  }
  else {
    if(cloneRepo) {
      gitGlobal.setResult({
        printText: printText,
        result: cloneRepoResult,
        ready: cloneResult
      });
    }
    else {
     gitGlobal.setResult({
        printText: printText,
        result: repoResults,
        ready: cloneResult
      });
    }
  }
}

function _printPullResultHeading() {
  const headingText = '*** pull result ***';
  const line = '*'.repeat(headingText.length);
  Log.log(Clc.yellow(line));
  Log.log(Clc.yellow(headingText));
  Log.log(Clc.yellow(line));
}

function isRepo(path, cb) {
  const repoResult = {
    error: null,
    isFolder: false,
    isRepo: false,
    success: () => {
      return null === repoResult.error && repoResult.isFolder && repoResult.isRepo;
    }
  };
  Fs.lstat(path, (err, stat) => {
    if(err) {
      if('ENOENT' === err.code) {
        cb(repoResult);
      }
      else {
        repoResult.error = err;
        cb(err, repo);
      }
    }
    else if(stat.isDirectory()) {
      repoResult.isFolder = true;
      GitSimple(path).checkIsRepo((err, result) => {
        if(err) {
          repoResult.error = err;
        }
        else {
          repoResult.isRepo = true;
        }
        cb(repoResult);
      });
    }
    else {
      cb(repoResult);
    }
  });
}

function getRepoStatus(path, repoGroup, name, cb) {
  if(!repoGroup) {
    return process.nextTick(() => {
      cb();
    });
  }
  const repo = {
    name: name,
    path: path,
    error: null,
    isFolder: false,
    isRepo: false,
    ready: false,
    result: null
  };
  repoGroup.repos.push(repo);
  Fs.lstat(path, (err, stat) => {
    if(err) {
      if('ENOENT' == err.code) {
        cb(undefined, repo);
      }
      else {
        repo.error = err;
        cb(err, repo);
      }
    }
    else if(stat.isDirectory()) {
      repo.isFolder = true;
      GitSimple(path).checkIsRepo((err, result) => {
        if(err) {
          repo.error = err;
          return cb(err, repo);
        }
        if(result) {
          repo.isRepo = true;
          GitSimple(path).fetch((err, result) => {
            if(err) {
              repo.error = err;
              return cb(err, repo);
            }
            GitSimple(path).status((err, result) => {
              repo.error = err;
              repo.ready = 0 === result.not_added.length && 0 === result.conflicted.length && 0 === result.created.length && 0 === result.deleted.length && 0 === result.modified.length && 0 === result.renamed.length && 0 === result.staged.length && 0 === result.ahead && 0 == result.behind;
              repo.result = result;
              cb(undefined, repo);
            });
          });
        }
        else {
          cb(undefined, repo);
        }
      });
    }
    else {
      cb(undefined, repo);
    }
  });
}

function createRepoData() {
  const repoGroups = [];
  let build;
  let development;
  let content;
  let data;
  let documentation;
  _RepoManager.repos.repoGroups.forEach((repoGroup) => {
    if('build' === repoGroup.name) {
      if(!build) {
        build = {
          name: 'Build',
          repos: [],
          result: null
        };
        repoGroups.push(build);
      }
    }
    else if('development' === repoGroup.name) {
      if(!development) {
        development = {
          name: 'Development',
          repos: [],
          result: null
        };
        repoGroups.push(development);
      }
    }
    else if('content' === repoGroup.name) {
      if(!content) {
        content = {
          name: 'Content',
          repos: [],
          result: null
        };
        repoGroups.push(content);
      }
    }
    else if('data' === repoGroup.name) {
      if(!data) {
        data = {
          name: 'Data',
          repos: [],
          result: null
        };
        repoGroups.push(data);
      }
    }
    else if('documentation' === repoGroup.name) {
      if(!documentation) {
        documentation = {
          name: 'Documentation',
          repos: [],
          result: null
        };
        repoGroups.push(documentation);
      }
    }
  });
  const result = {
    repoGroups
  };
  if(build) {
    result.build = build;
  }
  if(development) {
    result.development = development;
  }
  if(content) {
    result.content = content;
  }
  if(data) {
    result.data = data;
  }
  if(documentation) {
    result.documentation = documentation;
  }
  return result;
}

Tr.task('task:status', (cb) => {
  _RepoManager.init((err) => {
    if(err) {
      return cb(err);
    }
    try {
      const releaseData = Reflect.get(global, 'release-data@abstractor');
      const dynamicConfig = Reflect.get(global, 'dynamicConfig@abstractor');
      const statusRepo = dynamicConfig.parameters.get('repo');
      const repoData = createRepoData() ;
      const dirTop = 'delivery' === releaseData.releaseStep ? `${process.cwd()}${Path.sep}..${Path.sep}..${Path.sep}..` : `${process.cwd()}${Path.sep}..`;
      Fs.readdir(dirTop, (err, files) => {
        if(err) {
          Log.log(err);
          return;  
        }
        let pendings = 0;
        files.forEach((file) => {
          if(file.startsWith('actor') || file.startsWith('persistant-node-') || file.startsWith('node-')) {
            const path = `${dirTop}${Path.sep}${file}`;
            ++pendings;
            if(undefined === statusRepo || file === statusRepo) {
              getRepoStatus(path, repoData.build, file, () => {
                if(0 === --pendings) {
                  printRepoStatus(repoData.repoGroups);
                  cb();
                }
              });
            }
            else {
              process.nextTick(() => {
                if(0 === --pendings) {
                  printRepoStatus(repoData.repoGroups);
                  cb();
                } 
              });
            }
          }
          else if('Content' === file) {
            ++pendings;
            Fs.readdir(`${dirTop}${Path.sep}${file}`, (err, files) => {
              if(err) {
                if(0 === --pendings) {
                  Log.log(err);
                  printRepoStatus(repoData.repoGroups);
                  cb(err);
                }
                return;
              }
              files.forEach((file) => {
                ++pendings;
                if(undefined === statusRepo || file === statusRepo) {
                   getRepoStatus(`${dirTop}${Path.sep}Content${Path.sep}${file}`, repoData.content, file, () => {
                    if(0 === --pendings) {
                      printRepoStatus(repoData.repoGroups);
                      cb();
                    }
                  });
                }
                else {
                  process.nextTick(() => {
                    if(0 === --pendings) {
                      printRepoStatus(repoData.repoGroups);
                      cb();
                    } 
                  });
                }
              });
              if(0 === --pendings) {
                printRepoStatus(repoData.repoGroups);
                cb();
              }
            });
          }
          else if('Data' === file) {
            ++pendings;
            Fs.readdir(`${dirTop}${Path.sep}${file}`, (err, files) => {
              if(err) {
                if(0 === --pendings) {
                  Log.log(err);
                  printRepoStatus(repoData.repoGroups);
                  cb(err);
                }
                return;
              }
              files.forEach((file) => {
                ++pendings;
                if(undefined === statusRepo || file === statusRepo) {
                  getRepoStatus(`${dirTop}${Path.sep}Data${Path.sep}${file}`, repoData.data, file, () => {
                    if(0 === --pendings) {
                      printRepoStatus(repoData.repoGroups);
                      cb();
                    }
                  });
                }
                else {
                  process.nextTick(() => {
                    if(0 === --pendings) {
                      printRepoStatus(repoData.repoGroups);
                      cb();
                    } 
                  });
                }
              });
              if(0 === --pendings) {
                printRepoStatus(repoData.repoGroups);
                cb();
              }
            });
          }
          else if('Documentation' === file) {
            ++pendings;
            Fs.readdir(`${dirTop}${Path.sep}${file}`, (err, files) => {
              if(err) {
                if(0 === --pendings) {
                  Log.log(err);
                  printRepoStatus(repoData.repoGroups);
                  cb(err);
                }
                return;
              }
              files.forEach((file) => {
                ++pendings;
                if(undefined === statusRepo || file === statusRepo) {
                  getRepoStatus(`${dirTop}${Path.sep}Documentation${Path.sep}${file}`, repoData.documentation, file, () => {
                    if(0 === --pendings) {
                      printRepoStatus(repoData.repoGroups);
                      cb();
                    }
                  });
                }
                else {
                  process.nextTick(() => {
                    if(0 === --pendings) {
                      printRepoStatus(repoData.repoGroups);
                      cb();
                    } 
                  });
                }
              });
              if(0 === --pendings) {
                printRepoStatus(repoData.repoGroups);
                cb();
              }
            });
          }
          else if(file.startsWith('z-abs') || file.startsWith('z-action') || file.startsWith('z-plugin') || file.startsWith('z-component-plugin') || file.startsWith('z-service') || file.startsWith('z-build')|| file.startsWith('z-database')) {
            const path = `${dirTop}${Path.sep}${file}`;
            ++pendings;
            if(undefined === statusRepo || file === statusRepo) {
              getRepoStatus(path, repoData.development, file, () => {
                if(0 === --pendings) {
                  printRepoStatus(repoData.repoGroups);
                  cb();
                }
              });
            }
            else {
              process.nextTick(() => {
                if(0 === --pendings) {
                  printRepoStatus(repoData.repoGroups);
                  cb();
                } 
              });
            }
          }
        });
      });
    }
    catch(err) {
      Log.log(Clc.red('EXCEPTION:'), err);
    }
  }, false);
});

Tr.task('task:clone', (cb) => {
  _RepoManager.init((err) => {
    if(err) {
      return cb(err);
    }
    try {
      const cloneAll = gitGlobal.getAll();
      const cloneRepo = gitGlobal.getRepo();
      let cloneRepoReady = false;
      let cloneRepoResult = null;

      let argumentErrors = [];
      if(!cloneRepo && !cloneAll) {
        argumentErrors.push('--repo [value] or --all must be set.');
      }

      if(0 !== argumentErrors.length) {
        let errorText = Clc.red('Clone error:');
        argumentErrors.forEach((argumentError) => {
          errorText += '  ' + argumentError;
        });
        Log.log(errorText);
        return cb(new Error(errorText));
      }

      Fs.readFile(_RepoManager.getPath(), (err, data) => {
        if(err) {
          const errText = `Could not read file: '${_RepoManager.getPath()}'`;
          Log.err(errText);
          return cb(new Error(errText));
        }
        try {
          const repoDatas = JSON.parse(data);
          const repoResults = createRepoData();
          let ready = true;
          let pendings = 0;
          let reposToClone = false;
          repoDatas.repoGroups.forEach((repoGroup) => {
            repoGroup.repos.forEach((repoData) => {
              reposToClone = true;
              ++pendings;
              if(cloneAll || cloneRepo === repoData.folder) {
                const repoResult = Reflect.get(repoResults, repoGroup.name);
                repoResult.result = {
                  folderExists: [],
                  repoExists: [],
                  repoSucceded: [],
                  repoFailed: []
                };
                getRepoStatus(`${Path.normalize(repoGroup.path)}${Path.sep}${repoData.folder}`, repoResult, repoData.folder, (err, repoStatus) => {
                  if(cloneRepo === repoStatus.name || cloneAll) {
                    if(repoStatus.isFolder) {
                      if(repoStatus.isRepo) {
                        repoResult.result.repoExists.push(repoStatus);
                      }
                      else {
                        repoResult.result.folderExists.push(repoStatus);
                      }
                      ready = ready && repoStatus.ready && repoStatus.isRepo;
                      if(cloneRepo === repoStatus.name) {
                        cloneRepoReady = repoStatus.ready;
                        cloneRepoResult = repoStatus;
                      }
                    }
                    else {
                      Log.log('start clone repo:', repoData.repo, '...');
                      ++pendings;
                      GitSimple().clone(repoData.repo, `${Path.normalize(repoGroup.path)}${Path.sep}${repoData.folder}`, (err, result) => {
                        if(!err) {
                          repoResult.result.repoSucceded.push(repoStatus);
                          cloneRepoReady = true;
                        }
                        else {
                          Log.log(Clc.red(`Failed to clone repo `) + `${Path.normalize(repoGroup.path)}${Path.sep}${repoData.folder}`, err);
                          repoResult.result.repoFailed.push(repoStatus);
                        }
                        ready = ready && !err;
                        if(cloneRepo === repoStatus.name) {
                          cloneRepoReady = repoStatus.ready;
                          cloneRepoResult = repoStatus;
                        }
                        if(0 === --pendings) {
                          gitGlobal.setReady(cloneAll ? ready : cloneRepoReady);
                          printCloneResult(repoResults, cloneRepoResult, cloneRepo);
                          cb();
                        }
                      });
                    }
                  }
                  if(0 === --pendings) {
                    gitGlobal.setReady(cloneAll ? ready : cloneRepoReady);
                    printCloneResult(repoResults, cloneRepoResult, cloneRepo);
                    cb();
                  }
                });
              }
              else {
                process.nextTick(() => {
                  if(0 === --pendings) {
                    gitGlobal.setReady(cloneAll ? ready : cloneRepoReady);
                    printCloneResult(repoResults, cloneRepoResult, cloneRepo);
                    cb();
                  }
                });
              }
            });
          });
          if(!reposToClone) {
            printCloneResult(repoResults, cloneRepoResult, cloneRepo);
            cb();
          }
        } catch(err) {
          Log.log(Clc.red('EXCEPTION'), err);
          cb();
        }
      });
    }
    catch(err) {
      Log.log(Clc.red('EXCEPTION:'), err);
    }
  }, false);
});

Tr.task('task:pull', (cb) => {
  _RepoManager.init((err) => {
    if(err) {
      return cb(err);
    }
    try {
      const tagAll = gitGlobal.getAll();
      const tagRepo = gitGlobal.getRepo();

      let argumentErrors = [];
      if(!tagRepo && !tagAll) {
        argumentErrors.push('--repo [value] or --all must be set.');
      }

      if(0 !== argumentErrors.length) {
        let errorText = Clc.red('git pull error:');
        argumentErrors.forEach((argumentError) => {
          errorText += '  ' + argumentError;
        });

        Log.log(errorText);
        return cb(new Error(errorText));
      }

      if(tagRepo) {
        const repoData = _RepoManager.get(tagRepo);
        if(repoData) {
          GitSimple(Path.normalize(`${repoData.path}${Path.sep}${repoData.data.folder}`)).pull((err, update) => {
            _printPullResultHeading();
            if(err) {
              Log.log(Clc.red('git pull error:'), err);
            }
            else {
              Log.log(Clc.green('git pull'), Path.normalize(`${repoData.path}${Path.sep}${repoData.data.folder}`));
            }
            cb(err);
          });
        }
        else {
          cb();
        }
      }
      else {
        let pendings = 0;
        const errors = [];
        let reposToPull = false;
        _printPullResultHeading();
        _RepoManager.repos.repoGroups.forEach((repoGroup) => {
          repoGroup.repos.forEach((repo) => {
            reposToPull = true;
            ++pendings;
            const folder = Path.normalize(`${repoGroup.path}${Path.sep}${repo.folder}`);
            isRepo(folder, (result) => {
              if(result.success()) {
                GitSimple(folder).pull((err, update) => {
                  if(err) {
                    Log.log(Clc.red('git pull error:'), err);
                    errors.push(Clc.red('git pull error:'), err);
                  }
                  else {
                    Log.log(Clc.green('git pull'), Path.normalize(`${repoGroup.path}${Path.sep}${repo.folder}`));
                  }
                  if(0 === --pendings) {
                    cb(0 !== errors.length ? new Error(errors.join('\r\n')) : undefined);
                  }
                });
              }
              else {
                if(result.error) {
                  Log.log(Clc.red(`Path ${folder} error: ${result.error}`));
                }
                else if(!result.isFolder) {
                  Log.log(Clc.yellow(`Path ${folder} does not exist.`));
                }
                else if(!result.isRepo) {
                  Log.log(Clc.yellow(`Path ${folder} is not a repo.`));
                }
                if(0 === --pendings) {
                  cb(0 !== errors.length ? new Error(errors.join('\r\n')) : undefined);
                }
              }
            });
          });
        });
        if(!reposToPull) {
          cb();
        }
      }
    }
    catch(err) {
      Log.log(Clc.red('EXCEPTION:'), err);
    }
  }, false);
});
