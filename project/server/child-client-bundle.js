
'use strict';

const ChildBuildData = require('./child-build-data');
const Log = require('./log');
const Babelify = require('babelify');
const Browserify = require('browserify');
const Clc = require('cli-color');
const FastGlob = require('fast-glob');
const Babel = require("@babel/core");
const Fs = require('fs');
const Path = require('path');
const Zlib = require('zlib');


class ChildClientBundle {
  static START = './build/'.length;
  static STOP = '.js'.length;
  
  constructor(appName, name, silent, bundleName, source, dest, ignores, externals, externalGlobs, exports, exportSource, shim, nodeEnv, buildEnv) {
    this.appName = appName;
    this.name = name;
    this.silent = silent;
    this.bundleName = bundleName;
    this.source = source;
    this.dest = dest;
    this.ignores = ignores;
    this.externals = externals;
    this.externalGlobs = externalGlobs;
    this.exports = exports;
    this.exportSource = exportSource;
    this.shim = shim;
    process.env.NODE_ENV = nodeEnv;
    this.production = 1 === buildEnv;
  }
  
  task(cb) {
    const buildDataPath = Path.resolve(`..${Path.sep}Generated${Path.sep}Build${Path.sep}${this.appName}-${this.name.replaceAll(':', '-').replaceAll('/', '-')}`);
    ChildBuildData.getBuildData(buildDataPath, (buildData) => {
      let pendings = 3;
      let changed = false;
      let sources = null;
      let externals = null;
      const dstFile = `${this.dest.replaceAll('/', Path.sep)}${Path.sep}${this.bundleName}`;
      Fs.lstat(dstFile, (err, stat) => {
        if(!!err) {
          changed = 'ENOENT' === err.code;
        }
        if(0 === --pendings) {
          if(changed) {
            this._build(sources, externals, buildDataPath, buildData, cb);
          }
          else {
            cb();
          }
        }
      });
      this._sourceChanged(buildData, (_changed, _sources) => {
        changed = changed || _changed;
        sources = _sources;
        if(0 === --pendings) {
          if(changed) {
            this._build(sources, externals, buildDataPath, buildData, cb);
          }
          else {
            cb();
          }
        }
      });
      this._externalChanged(buildData, (_changed, _externals) => {
        changed = changed || _changed;
        externals = _externals;
        if(0 === --pendings) {
          if(changed) {
            this._build(sources, externals, buildDataPath, buildData, cb);
          }
          else {
            cb();
          }
        }
      });
    });
  }
  
  _build(sources, externals, buildDataPath, buildData, cb) {
    const browserify = Browserify({
      entries: sources,
      debug: false,
      ignoreMissing: true
    }, this.shim);
    if(this.ignores) {
      this.ignores.forEach((ignore) => {
        browserify.ignore(ignore);
      });
    }
    
    if(null !== this.externals) {
      this.externals.forEach((external) => {
        browserify.external(external);
      });
    }
    
    if(null !== this.exports) {
      this.exports.forEach((export_) => {
        if(!Array.isArray(export_)) {
          browserify.require(export_);
        }
        else {
          if(this.production) {
            browserify.require(export_[0][0], {expose: export_[0][1]});
          }
          else {
            browserify.require(export_[1][0], {expose: export_[1][1]});
          }
        }
      });
    }
    
    sources.forEach((source) => {
      browserify.require(source, {expose: source.substring(ChildClientBundle.START, source.length - ChildClientBundle.STOP)});
    });
    
    externals.forEach((external) => {
      browserify.external(external.substring(ChildClientBundle.START, external.length - ChildClientBundle.STOP));
    });
   
    this._bundleClientBundle(browserify, buildDataPath, buildData, cb);
  }
  
  async _sourceChanged(buildData,  cb) {
    if(this.source && 0 !== this.source.length) {
      let pendings = this.source.length;
      const entries = [];
      let changed = false;
      for(let i = 0; i < this.source.length; ++i) {
        const files = await FastGlob([this.source[i]], { caseSensitiveMatch: true, braceExpansion: false });
        entries.push(files);
        files.forEach((file) => {
          ++pendings;
          Fs.lstat(file, (err, stat) => {
            const mTimeMs = buildData.map.get(file);
            if(mTimeMs !== stat.mtimeMs) {
              changed = true;
              ChildBuildData.set(buildData, file, stat.mtimeMs);
            }
            if(0 === --pendings) {
              cb(changed, entries.flat());
            }
          });
        });
        if(0 === --pendings) {
          cb(changed, entries.flat());
        }
      }
    }
    else {
      process.nextTick(() => {
        cb(false, []);
      });
    }
  }
  
  async _externalChanged(buildData, cb) {
    if(this.externalGlobs && 0 !== this.externalGlobs.length) {
      let pendings = 0;
      const entries = [];
      let changed = false;
      for(let i = 0; i < this.externalGlobs.length; ++i) {
        ++pendings;
        const files = await FastGlob([this.externalGlobs[i]], { caseSensitiveMatch: true, braceExpansion: false });
        if(0 === --pendings) {
          cb(changed, entries.flat());
          return;
        }
        entries.push(files);
        files.forEach((file) => {
          ++pendings;
          Fs.lstat(file, (err, stat) => {
            const mTimeMs = buildData.map.get(file);
            if(mTimeMs !== stat.mtimeMs) {
              changed = true;
              ChildBuildData.set(buildData, file, stat.mtimeMs);
            }
            if(0 === --pendings) {
              cb(changed, entries.flat());
              return;
            }
          });
        });
      }
      if(0 === pendings) {
        cb(changed, entries.flat());
      }
    }
    else {
      process.nextTick(cb.bind(this, false, []));
    }
  }
  
  _bundleClientBundle(browserify, buildDataPath, buildData, cb) {
    browserify
    .on('error', console.error.bind(console))
    .transform(Babelify, {
      plugins: ["@babel/plugin-transform-runtime"],
      presets: ["@babel/preset-env"],
      compact: this.production,
      minified: this.production,
      comments: !this.production
    })
    .bundle((err, buf) => {
      if(err) {
        Log.log('-----------------------------------------------------------------------------', buf);
        Log.log(`Bundle ${this.name} error:`, err);
        cb();
        return;
      }
      if(this.production) {
        Babel.transform(buf, {
          compact: true,
          minified: true,
          comments: false
        }, (err, result) => {
          const dstFile = `${this.dest}${Path.sep}${this.bundleName}`;
          let pendings = 2;
          Zlib.gzip(result.code, (err, data) =>  {
            if(err) {
              Log.log(`Bundle Zip ${this.name} error:`, err);
              if(0 === --pendings) {
                cb();
              }
              return;
            }
            Fs.writeFile(dstFile + '.gzip', data, (err) => {
              if(err) {
                Log.log(Clc.red(`Could not write: '`) + dstFile + Clc.red(`'.`), err);
              }
              if(0 === --pendings) {
                ChildBuildData.setBuildData(buildDataPath, buildData, cb);
              }
            });
          });
          Fs.writeFile(dstFile, result.code, (err) => {
            if(err) {
              Log.log(Clc.red(`Could not write: '`) + dstFile + Clc.red(`'.`), err);
            }
            if(0 === --pendings) {
              ChildBuildData.setBuildData(buildDataPath, buildData, cb);
            }
          });
        });
      }
      else {
        const dstFile = `${this.dest}${Path.sep}${this.bundleName}`;
        Fs.writeFile(dstFile, buf, (err) => {
          if(err) {
            Log.log(Clc.red(`Could not write: '`) + dstFile + Clc.red(`'.`), err);
          }
          ChildBuildData.setBuildData(buildDataPath, buildData, cb);
        });
      }
    });
  }
}


process.nextTick(() => {
  const parameters = JSON.parse(process.argv[2]);
  const childClientBundle = new ChildClientBundle(...parameters);
  childClientBundle.task(() => {
    process.send(0);
  });
});
