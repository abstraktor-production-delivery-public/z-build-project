
'use strict';

const Log = require('./log');
const TaskRunner = require('./task-runner');
Reflect.set(global, 'tr@abstractor', new TaskRunner());
const Clc = require('cli-color');
const Fs = require('fs');
const Path = require('path');


class AbsProcess {
  static _run(appName, releaseStep, organization, settings, previousArg) {
    const releaseData = {
      appName: appName,
      releaseStep: releaseStep,
      organization: organization,
      packageJson: null,
      workspace: null,
      env: ''
    };
    Reflect.set(global, 'release-data@abstractor', releaseData);
    const dynamicConfig = Reflect.get(global, 'dynamicConfig@abstractor');
    dynamicConfig.app = appName;
    const buildAll = dynamicConfig.parameters.has('buildall');
    const install = dynamicConfig.parameters.has('install');
    const build = dynamicConfig.parameters.has('build') || buildAll;
    const dev = dynamicConfig.parameters.has('dev');
    const bottleneck = dynamicConfig.parameters.get('bottleneck');
    const Tr = Reflect.get(global, 'tr@abstractor');
    const serverName = settings.type ? `${settings.type}-${settings.name}` : settings.name;
    Tr.task(serverName, (cbTask) => {
      process.title = serverName;
      const Node = require('z-abs-corelayer-server/server/node/node');
      AbsProcess.process = new Node(releaseData, dynamicConfig, settings, build);
      Tr.task(AbsProcess.process.nodeName, AbsProcess.process.run.bind(AbsProcess.process));
      AbsProcess.process.run(cbTask);
    });
    let Workspace;
    try {
      Workspace = !buildAll ? require('./workspace') : undefined;
    }
    catch(err) {
      return Log.log(Clc.red('Could not create'), `require('./workspace')`);
    }
    let BuildAll;
    try {
      BuildAll = buildAll ? require('./build-all') : undefined;
    }
    catch(err) {
      return Log.log(Clc.red('Could not create'), `require('./build-all')`);
    }
    let NpmInstallAll;
    try {
      NpmInstallAll = install ? require('./npm-install-all') : undefined;
    }
    catch(err) {
      return Log.log(Clc.red('Could not create'), `require('./npm-install-all')`);
    }
    let workspace = null;
    const silent = true;
    const npmInstall = (cb) => {
      if(install) {
        try {
          const npmInstallAll = new NpmInstallAll(silent);
          npmInstallAll.run(() => {
            cb();
          });
        }
        catch(err) {
          Log.log(serverName, Clc.red('npm install all error: '), err);
          return cb(err);
        }
      }
      else {
        cb();
      }
    };
    const readPackageJson = (cb) => {
      const packageJsonPath = `.${Path.sep}package.json`;
      Fs.readFile(packageJsonPath, (err, data) => {
        let packageJson = '';
        try {
          packageJson = JSON.parse(data);
        }
        catch(err) {
          Log.log(Clc.red('Failed to parse package.json'), err);
          return cb(err);
        }
        if(!err) {
          releaseData.packageJson = packageJson;
        }
        cb(err);
      });
    };
    let coreTask = 'default';
    let task = 'default';
    for(let i = 0; i < dynamicConfig.tasks.length; ++i) {
      task = dynamicConfig.tasks[i];
      if('debug' === task || 'redebug' === task || 'release' === task || 'rerelease' === task) {
        coreTask = task;
        if('release' === task || 'rerelease' === task)  {
          releaseData.env = 'production';
        }
        else {
          releaseData.env = 'development';
        }
        break;
      }
    }
    let nextArg = previousArg;
    if('default' === task) {
      if('debug' === previousArg) {
        releaseData.env = 'development';
      }
      else if('release' === previousArg) {
        releaseData.env = 'production';
      }
      else {
        task = 'release';
        releaseData.env = 'production';
        nextArg = 'release';
      }
    }
    else if('debug' === task) {
      if('release' === previousArg) {
        task = 'redebug';
        nextArg = 'debug';
      }
    }
    else if('release' === task) {
      if('debug' === previousArg) {
        task = 'rerelease';
        nextArg = 'release';
      }
    }
    else if('redebug' === task) {
      nextArg = 'debug';
    }
    else if('rerelease' === task) {
      nextArg = 'release';
    }
    process.env.NODE_ENV = Reflect.get(global, 'release-data@abstractor').env;
    const path = `.${Path.sep}`;
    Fs.writeFile(`${path}actorjs.arg`, nextArg, (err) => {});
    if(!buildAll) {
      Tr.task(coreTask, Tr.serial(Tr.parallel(npmInstall, readPackageJson), (cb) => {
        try {
          if(null === workspace) {
            workspace = new Workspace(appName, settings.name, coreTask, build, dev, organization, bottleneck, silent);
            releaseData.workspace = workspace;
          }
          workspace.load((err) => {
            if(err) {
              Log.log('Could not start the Node:', err);
              return cb(err);
            }
            cb();
          }, `task:${task}`);
        }
        catch(err) {
          Log.log(serverName, Clc.red('buildall error: '), err);
          return cb(err);
        }
      }));
    }
    else {
      Tr.task(coreTask, Tr.serial(npmInstall, (cb) => {
        try {
          const buildAll = new BuildAll(coreTask, build, dev, organization, bottleneck, silent);
          buildAll.run(() => {
            cb();
          });
        }
        catch(err) {
          Log.log(serverName, Clc.red('buildall error: '), err);
          return cb(err);
        }
      }));
    }
    const GitTasks = require('./git-tasks');
    const ServiceCommands = require('./service/commands');
    Tr.run(coreTask);
  }
  
  static run(appName, releaseStep, organization) {
    let pendings = 2;
    let settings = '';
    let previousArg = '';
    const path = `.${Path.sep}`;
    Fs.readFile(`${path}settings.json`, (err, data) => {
      if(err) {
        Log.error(err);
        return;
      }
      settings = JSON.parse(data);
      if(0 === --pendings) {
        AbsProcess._run(appName, releaseStep, organization, settings, previousArg);
      }
    });
    Fs.readFile(`${path}actorjs.arg`, (err, data) => {
      if(!err) {
        previousArg = data.toString();
      }
      if(0 === --pendings) {
        AbsProcess._run(appName, releaseStep, organization, settings, previousArg);
      }
    });
  }
}


AbsProcess.process = null;


module.exports = AbsProcess;
