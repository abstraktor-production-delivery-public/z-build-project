
'use strict';

const Log = require('./log');
const Clc = require('cli-color');
const Tr = Reflect.get(global, 'tr@abstractor');
const Fs = require('fs');


class Task {
  constructor(part, fileName) {
    this.name = `task:${part}`;
    this.fileName = part.replace(/[/\\?%*:|"<>]/g, '-');
    this.requireName = fileName.substring(__dirname.length + 1, fileName.length - '.js'.length);
  }
  
  static load(fileName, requireName, cb) {
    Fs.readFile(`./workspace/${fileName}.tsk`, (err, data) => {
      if(err) {
        return cb(err);
      }
      const dataObject = JSON.parse(data);
      const keys = Reflect.ownKeys(dataObject);
      const parameters = [];
      let dependencyNames = [];
      keys.forEach((key) => {
        if('dependencyNames' !== key) {
          parameters.push(Reflect.get(dataObject, key));
        }
        else {
          dependencyNames = Reflect.get(dataObject, key);
        }
      });
      const P = require('./' + requireName);
      cb(err, new P(...parameters), dependencyNames);
    });
  }
  
  save() {
    Fs.writeFile(`./workspace/${this.fileName}.tsk`, JSON.stringify(this.saveData(), null, 2), (err) => {
      if(err) {
        Log.log('ERROR:'. err);
      }
    });
  }
  
  init() {
    Tr.task(this.name, this.executeTask.bind(this));
  }
  
  executeTask(cb) {
    this.task((err) => {
      cb(err);
    });
  }
}


module.exports = Task;
