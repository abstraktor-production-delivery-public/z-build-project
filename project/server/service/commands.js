
//'use strict';

let NodeAdmin = null;
const Clc = require('cli-color');
const Tr = Reflect.get(global, 'tr@abstractor');


class Commands {
  static instance = null;
  static HEADING_SIZE = 6;
  static COLUMNS = ['name', 'shortName', 'state', 'pid', 'servers', 'clients', 'ping',  'statusTicks', 'type', 'status'];
  static COLUMNS_SIZE = ['name'.length + 1, 'shortName'.length + 1, 'state'.length + 1, 'pid'.length + 1, 'servers'.length + 1, 'clients'.length + 1, 'ping'.length + 1, 'statusTicks'.length + 1, 'type'.length + 1, 'status'.length + 1];
  
  constructor(debug=false) {
    this.debug = debug;
    this.host = 'localhost';
    this.port = 9011;
    this.nodeAdmin = new NodeAdmin(this.host, this.port, this._printTable.bind(this));
    const ws = process.stdout.getWindowSize();
    this.columns = ws[0];
    this.rows = ws[1];
    this.x = 0;
    this.y = 0;
    this.i = -1;
    this.input = '';
    this.inputRow = 0;
    this.connectionColumn = 0;
    this.intervalId = -1;
    this.previousTableWidth = 0;
  }
  
  start() {
    const connectionText = `tcp://${this.host}:${this.port}`;
    this.connectionColumn = connectionText.length;
    process.stdout.on('resize', () => {
      this._clearScreen(() => {
        process.stdout.cursorTo(0, 0);
        console.log(Clc.yellow('***************************************************'));
        console.log(Clc.yellow(`*** services ***`), 'uptime: 0');
        console.log(Clc.yellow('***************************************************'));
        console.log(Clc.yellow(`*** Connection ***`), connectionText, '-', Clc.red('DOWN'));
        console.log(Clc.yellow('***'));
        console.log(Clc.yellow('***************************************************'));
        this._restoreCursor();
      });
    });
    this._clearScreen(() => {
      console.log(Clc.yellow('***************************************************'));
      console.log(Clc.yellow(`*** services ***`), 'uptime: 0');
      console.log(Clc.yellow('***************************************************'));
      console.log(Clc.yellow(`*** Connection ***`), connectionText, '-', Clc.red('DOWN'));
      console.log(Clc.yellow('***'));
      console.log(Clc.yellow('***************************************************'));
      process.stdin.setRawMode(true);
      process.stdin.on('data', (chunk) => {
        process.stdin.resume();
        if(3 === chunk[0]) {
          clearInterval(this.intervalId);
          this._setCursor(0, this.inputRow);
          process.stdout.clearLine(1, () => {
            process.exit();
          });
          return;
        }
        else if(13 === chunk[0]) {
          this._handleCommand(this.input, () => {
            this.input = '';
            process.stdout.clearLine(1, () => {
              this._setCursor(0, this.inputRow);
              console.log('>', this.input);
              this._setCursor(2 + this.input.length, this.inputRow);
            });
          });
          return;
        }
        else if(8 === chunk[0]) {
          if(0 !== this.input.length) {
            this.input = this.input.substring(0, this.input.length - 1);
          }
        }
        else {
          this.input += chunk;  
        }
        this._setCursor(0, this.inputRow);
        console.log('>', this.input);
        process.stdout.clearLine(1, () => {
     //     this._setCursor(0, this.inputRow, () => {
       //     process.stdout.clearLine(1, () => {
              //console.log(chunk[0]);
              this._setCursor(2 + this.input.length, this.inputRow);
            });
     //     });
      //  });
      });
      this._poll();
    });
  }
  
  _printTable() {
    if(!this.debug) {
      try {
        const sizes = [...Commands.COLUMNS_SIZE];
        this.nodeAdmin.nodeDatas.forEach((nodeData) => {
          Commands.COLUMNS.forEach((column, index) => {
            const value = Reflect.get(nodeData, column);
            if('string' === typeof value) {
              if(value.length > sizes[index]) {
                sizes[index] = value.length;
              }
            }
            else {
             const v  = value.toString();
             if(v.length > sizes[index]) {
                sizes[index] = v.length;
              }
            }
          });
        });
        const sum = sizes.reduce((accumulator, currentValue) => accumulator + currentValue);
        const diff = Math.max(0, this.previousTableWidth - sum);
        this.previousTableWidth = sum;
        this._setCursor(0, Commands.HEADING_SIZE);
        
        let firstRow = '┌────┬';
        for(let i = 0; i < sizes.length - 1; ++i) {
          firstRow += '─'.repeat(sizes[i] + 2);
          firstRow += '┬';
        }
        firstRow += '─'.repeat(sizes[sizes.length - 1]);
        firstRow += '──┐' + ' '.repeat(diff);
  
        firstRow += '\r\n│ #  │ ';
        for(let i = 0; i < sizes.length; ++i) {
          firstRow += Commands.COLUMNS[i] + ' '.repeat(sizes[i] - Commands.COLUMNS[i].length) + ' │ ';
        }
        firstRow += ' '.repeat(diff);
        
        firstRow += '\r\n├────┼';
        for(let i = 0; i < sizes.length - 1; ++i) {
          firstRow += '─'.repeat(sizes[i] + 2);
          firstRow += '┼';
        }
        firstRow += '─'.repeat(sizes[sizes.length - 1]);
        firstRow += '──┤';
        firstRow += ' '.repeat(diff);
  
        this.nodeAdmin.nodeDatas.forEach((nodeData, index) => {
          firstRow += '\r\n│ ' + index + ' │ '.padStart(5 - index.toString().length, ' ');
          for(let i = 0; i < sizes.length; ++i) {
            const value = Reflect.get(nodeData, Commands.COLUMNS[i]);
            //const outputValue = 'string' === typeof value ? Clc.green(value) : Clc.yellow(value.toString());
            const outputValue = Commands.COLUMNS_VALUE[i](value);
            firstRow += outputValue + ' '.repeat(sizes[i] - ('string' === typeof value ? value.length : value.toString().length)) + ' │ ';
          }
          firstRow += ' '.repeat(diff);
        });
        firstRow += ' '.repeat(diff);
        
        firstRow += '\r\n└────┴';
        for(let i = 0; i < sizes.length - 1; ++i) {
          firstRow += '─'.repeat(sizes[i] + 2);
          firstRow += '┴';
        }
        firstRow += '─'.repeat(sizes[sizes.length - 1]);
        firstRow += '──┘';
        firstRow += ' '.repeat(diff);
        
        console.log(firstRow);
        
        this.inputRow = Commands.HEADING_SIZE + 4 + this.nodeAdmin.nodeDatas.length;
        this._setCursor(0, this.inputRow);
        console.log('>', this.input);
        this._setCursor(this.input.length + 2, this.inputRow);
      }
      catch(a) {
        console.log(Clc.red('_printTable'), a);
      }
    }
  }
  
  _pollImpl() {
    this._setCursor(25, 1, () => {
      if(!this.debug) {
        process.stdout.write("" + (++this.i));
      }
      this._restoreCursor(() => {
        this.nodeAdmin.check(() => {
          this._setCursor(22 + this.connectionColumn, 3, () => {
            if(!this.debug) {
              process.stdout.write(null !== this.nodeAdmin.server ? Clc.green('UP ') + this.nodeAdmin.serverUpDate : Clc.red('DOWN ') + this.nodeAdmin.serverDownReason);
            }
            this._setCursor(22 + this.connectionColumn, 4, () => {
              if(!this.debug) {
                process.stdout.write(this.nodeAdmin.serverStatus.connected ? Clc.green('CONNECTED - ') + this.nodeAdmin.serverStatus.connectedName : Clc.red('CONNECTION DOWN                              '));
              }
              this._restoreCursor(() => {
                this._printTable();
              });
            });
          });
        });
      });
    });
  }
  
  _handleCommand(input, cb) {
    const params = input.split(' ');
    let index = 0;
    while(index < params.length) {
      params[index] = params[index].trim();
      if('' === params[index]) {
        params.splice(index, 1);
      }
      else {
        ++index;
      }
    }
    if(1 === params.length) {
      if('start' === params[0]) {
        this.nodeAdmin.start((err) => {
          this._printTable();
        });
      }
      else if('stop' === params[0]) {
        this.nodeAdmin.stop((err) => {
          this._printTable();
        });
      }
    }
    else if(2 ===  params.length) {
      this.nodeAdmin.command(...params, () => {
        this._printTable();
      });
    }
    this._setCursor(0, this.inputRow, () => {
      process.stdout.clearLine(1, () => {
        this._setCursor(2 + this.input.length, this.inputRow, cb);
      });
    });
  }
  
  _poll() {
    this._pollImpl();
    this.nodeAdmin.startListen((err) => {});
    this.intervalId = setInterval(() => {
      this._pollImpl();
    }, 1000);
  }
  
  _setCursor(x, y, cb) {
    if(this.debug) {
      return cb && cb();
    }
    this.x = x;
    this.y = y;
    process.stdout.cursorTo(this.x, this.y, cb);
  }
  
  _restoreCursor(cb) {
    if(this.debug) {
      return cb && cb();
    }
    process.stdout.cursorTo(this.x, this.y, cb);
  }
  
  _clearScreen(cb) {
    if(this.debug) {
      return cb && cb();
    }
    process.stdout.cursorTo(0, 0, () => {
      process.stdout.clearScreenDown(() => {
        process.stdout.cursorTo(this.x, this.y, cb);
      });
    });
  }
}

Commands.COLUMNS_VALUE = [
  (value) => {return value;},
  (value) => {return value;},
  (value) => {
    switch(value) {
      case 'started':
        return Clc.green(value);
      case 'starting':
      case 'stopping':
        return Clc.yellow(value);
      default:
        return Clc.red(value);
    }
  },
  (value) => {return Clc.yellow(`${value}`);},
  (value) => {return Clc.green(value);},
  (value) => {return Clc.green(value);},
  (value) => {return Clc.yellow(`${value}`);},
  (value) => {return Clc.yellow(`${value}`);},
  (value) => {
    switch(value) {
      case 'local':
        return Clc.yellow(value);
      case 'node':
        return Clc.cyan(value);
      case 'persistant-node':
        return Clc.green(value);
      default:
        return Clc.red(value);
    }
  },
  (value) => {
    switch(value) {
      case 'on':
      case 'auto':
        return Clc.green(value);
      case 'off':
        return Clc.yellow(value);
      default:
        return Clc.red(value);
    }
  }
];
 
Tr.task('task:services', (cb) => {
  try {
    NodeAdmin = require('z-abs-corelayer-server/server/node/node-admin');
    Commands.instance = new Commands();
    Commands.instance.start();
  }
  catch(a) {
    console.log(a);
  }
});
