
'use strict';

const Log = require('./log');
const Clc = require('cli-color');
const FastGlob = require('fast-glob');
const Path = require('path');
const Fs = require('fs');


class TaskRunnerGlob {
  static async glob(source, dest, resolvedPath, done, cb) {
    let pendings = source.length;
    if(0 === pendings) {
      done();
      return;
    }
    for(let i = 0; i < source.length; ++i) {
      const src = source[i];
      const reqIndex = src.indexOf('*');
      const files = await FastGlob([src], { cwd: resolvedPath, caseSensitiveMatch: true, braceExpansion: false });
      files.forEach((file) => {
        ++pendings;
        const srcFile = Path.normalize(`${resolvedPath}${Path.sep}${file}`);
        let dstFile = '';
        if(-1 !== reqIndex) {
          dstFile = Path.normalize(`${dest}${Path.sep}${file.substring(reqIndex) }`);
        }
        else {
          const lastIndex = file.lastIndexOf('/');
          if(-1 === lastIndex) {
            Log.log(Clc.red(`File format wrong: '`) + file + Clc.red(`'. Searching for: '/'.`));
          }
          dstFile = Path.normalize(`${dest}${Path.sep}${file.substring(lastIndex) }`);
        }
        cb(srcFile, dstFile, () => {
          if(0 === --pendings) {
            done();
          }
        });
      });
      if(0 === --pendings) {
        done();
      }
    }
  }
  
  static verifyPath(file, cb) {
    Fs.access(file, Fs.constants.F_OK, (err) => {
      if(!err) {
        cb();
      }
      else if('ENOENT' === err.code) {
        const index = file.lastIndexOf(Path.sep);
        if(-1 !== index) {
          Fs.mkdir(file.substring(0, index), {recursive: true}, (err) => {
            if(err) {
              Log.log(Clc.red(`Fs.mkdir error: '`) + err + Clc.red('.'), err);
            }
            cb(err);
          });
        }
        else {
          Log.log(Clc.red(`File format wrong: '`) + file + Clc.red(`'. Searching for last: '${Path.sep}'.`));
          cb(new Error('File format wrong'));
        }
      }
      else {
        Log.log(Clc.red(`Fs.access error: '`) + err + Clc.red('.'), err);
        cb(err);
      }
    });
  }
}

module.exports = TaskRunnerGlob;
