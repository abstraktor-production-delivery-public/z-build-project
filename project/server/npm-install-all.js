
'use strict';

const Log = require('./log');
const Tr = Reflect.get(global, 'tr@abstractor');
const ChildProcess = require('child_process');
const Clc = require('cli-color');
const Fs = require('fs');
const Path = require('path');

const npmInstall = 'npm install';


class NpmInstallAll {
  constructor(silent) {
    this.silent = silent;
    this.npmInstallData = [];
    this.childProcess = null;
    this.basePath = `${process.cwd()}${Path.sep}..`;
  }
  
  run(cb) {
    Fs.readdir(this.basePath, (err, files) => {
      if(err) {
        return cb(err);
      }
      if(undefined === files || 0 === files.length) {
        return cb();
      }
      let pendings = 0;
      if(0 === files.length) {
        cb();
      }
      files.forEach((dir) => {
        Fs.lstat(`${this.basePath}${Path.sep}${dir}`, (err, stat) => {
          if(stat.isDirectory()) {
            if(dir.startsWith('actor') || dir.startsWith('node-') || dir.startsWith('persistant-node-')) {
              ++pendings;
              Fs.readFile(`${this.basePath}${Path.sep}${dir}${Path.sep}settings.json`, 'utf8', (err, data) => {
                let settings = null;
                if(!err) {
                  try {
                    settings = JSON.parse(data);
                  }
                  catch(err) {
                    Log.log('Could not parse settings.json for', dir);
                  }
                  this.npmInstallData.push({
                    name: dir,
                    settings: settings
                  });
                }
                else {
                  
                }
                if(0 === --pendings) {
                  this.npmInstallAll(cb);
                }
              });
            }
          }
        });
      });
    });
  }
  
  npmInstallAll(cb) {
    this.npmInstallData.sort((a, b) => {
      return a.name > b.name ? 1 : -1;
    });
    let pendings = this.npmInstallData.length;
    if(0 === pendings) {
      Log.log('Nothing to build.');
      return cb();
    }
    
    let index = 0;
    this.npmInstallHandler(index, cb)
  }
  
  npmInstallHandler(index, cb) {
    this.npmInstall(index, () => {
      if(++index !== this.npmInstallData.length) {
        this.npmInstallHandler(index, cb);
      }
      else {
        cb();
      }
    });    
  }
  
  npmInstall(index, cb) {
    const serverName = this.npmInstallData[index].settings.type ? `${this.npmInstallData[index].settings.type}-${this.npmInstallData[index].settings.name}` : this.npmInstallData[index].settings.name;
    Tr.serial((cbTask) => {
      Log.log(Clc.yellow('**************************************************'));
      Log.log(Clc.yellow(`*** ${npmInstall}: ${this.npmInstallData[index].name}`), Path.resolve(`${this.basePath}${Path.sep}${this.npmInstallData[index].name}`));
      Log.log(Clc.yellow('**************************************************'));
      try {
        this.childProcess = ChildProcess.exec(npmInstall, {cwd: Path.resolve(`${this.basePath}${Path.sep}${this.npmInstallData[index].name}`)});
        this.childProcess.on('exit', (code) => {
          cb();
        });
        this.childProcess.stdout.on('data', (data) => {
          const logs = data.split('\n');
          logs.forEach((log) => {
            if('' !== log.trim()) {
              Log.log(log);
            }
          });
        });
        this.childProcess.stderr.on('data', (data) => {
          const logs = data.split('\n');
          logs.forEach((log) => {
            if('' !== log.trim()) {
              Log.error(log);
            }
          });
        });
      }
      catch(err) {
        Log.log(Clc.red('npmInstall error:'), err);
        cb(err);
      }
    })();
  }
}


module.exports = NpmInstallAll;
