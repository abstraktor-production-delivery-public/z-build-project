
'use strict';

const Build = require('./build');
const Log = require('./log');
const ChildProcess = require('child_process');
const Path = require('path');
const Clc = require('cli-color');


class BuildClientJs extends Build {
  constructor(appName, layer, part, source, dest) {
    super(appName, `Client:${layer}Layer/${part}`, __filename, source, dest);
    this.layer = layer;
    this.part = part;
  }
  
  saveData() {
    return {
      layer: this.layer,
      part: this.part,
      source: this.source,
      dest: this.dest
    };
  }
  
  task(cb) {
    this.execute(0, 0, cb);
  }
  
  execute(retries, code, cb) {
    if(2 >= retries) {
      const childProcess = ChildProcess.fork(require.resolve('./child-client-js.js'), [JSON.stringify([this.appName, this.name, this.silent, this.repo, this.source, this.dest, process.env.NODE_ENV])], {execArgv: []});
      let done = false;
      childProcess.on('exit', (code) => {
        if(!done) {
          if(0 !== code) {
            return this.execute(++retries, code, cb);
          }
          done = true;
          cb();
        }
      });
      childProcess.on('message', (code) => {
        if(!done && 0 === code) {
          done = true;
          cb();
        }
      });
    }
    else {
      Log.log(Clc.red(`${this.name} - build-client-js: code`), code);
      cb(new Error(`Failed to build ${this.name} - build-client-js.`));
    }
  }
}

module.exports = BuildClientJs;
